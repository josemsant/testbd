<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscripcionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inscripciones', function(Blueprint $table){
			$table->bigIncrements('id');
			$table->unsignedBigInteger('id_alumno')->nullable();
			$table->unsignedBigInteger('id_curso')->nullable();
			$table->timestamps();

			$table->foreign('id_alumno','fk_alumno')->references('id')->on('alumnos')->onDelete('set null');
			$table->foreign('id_curso','fk_curso')->references('id')->on('cursos')->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('inscripciones', function(Blueprint $table){
			$table->dropForeign('fk_alumno');
			$table->dropForeign('fk_curso');
		});

		Schema::drop('inscripciones');
	}

}
