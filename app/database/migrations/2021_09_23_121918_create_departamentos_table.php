<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartamentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('departamentos', function(Blueprint $table){
			$table->bigIncrements('id');
			$table->string('nombre',50);
			$table->timestamps();
		});

		DB::statement('ALTER TABLE departamentos AUTO_INCREMENT = 3000;');
		DB::statement('ALTER TABLE departamentos ADD FULLTEXT nombre_departamento(nombre)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Schema::table('departamentos', function(Blueprint $table){
		// 	$table->dropForeign('fk_responsable');
		// });

		Schema::drop('departamentos');
	}

}
