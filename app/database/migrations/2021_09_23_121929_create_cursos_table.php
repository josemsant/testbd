<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cursos', function(Blueprint $table){
			$table->bigIncrements('id');
			$table->string('nombre',50);
			$table->decimal('precio',10,2);
			$table->integer('duracion');
			$table->integer('puntaje');
			$table->unsignedBigInteger('id_departamento')->nullable();
			$table->timestamps();
			
			$table->foreign('id_departamento','fk_departamento')->references('id')->on('departamentos')->onDelete('set null');
		});

		DB::statement('ALTER TABLE cursos AUTO_INCREMENT = 1220;');
		DB::statement('ALTER TABLE cursos ADD FULLTEXT nombre_curso(nombre)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cursos', function(Blueprint $table){
			$table->dropForeign('fk_departamento');
		});

		Schema::drop('cursos');
	}

}
