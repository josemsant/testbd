<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInscripcionesFkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('inscripciones', function(Blueprint $table){
			$table->dropForeign('fk_alumno');
			$table->foreign('id_alumno','fk_alumno')->references('id')->on('usuarios')->onDelete('set null');			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
