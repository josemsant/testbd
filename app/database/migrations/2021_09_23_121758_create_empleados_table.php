<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('empleados', function(Blueprint $table){
			$table->bigIncrements('id');
			$table->string('nombre',50);
			$table->string('dni',20);
			$table->string('telefono',20);
			$table->string('domicilio',50);
			$table->decimal('sueldo',11,2);
			$table->timestamps();
		});

		DB::statement('ALTER TABLE empleados AUTO_INCREMENT = 3060;');
		DB::statement('ALTER TABLE empleados ADD FULLTEXT nombre_empleado(nombre)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empleados');
	}

}
