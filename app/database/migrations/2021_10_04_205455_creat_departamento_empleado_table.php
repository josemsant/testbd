<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatDepartamentoEmpleadoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('departamentos_empleados', function(Blueprint $table){
			$table->bigIncrements('id');			
			$table->unsignedBigInteger('id_departamento')->nullable();
			$table->unsignedBigInteger('id_empleado')->nullable();
			$table->string('cargo',50);
			$table->timestamps();

			$table->foreign('id_departamento','fk_derpartamento_emp')->references('id')->on('departamentos')->onDelete('set null');
			$table->foreign('id_empleado','fk_empleado_dpto')->references('id')->on('empleados')->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('departamentos_empleados', function(Blueprint $table){
			$table->dropForeign('fk_derpartamento_emp');
			$table->dropForeign('fk_empleado_dpto');
		});

		Schema::drop('departamentos_empleados');
	}

}
