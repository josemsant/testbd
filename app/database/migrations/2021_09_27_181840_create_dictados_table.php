<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDictadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dictados', function(Blueprint $table){
			$table->bigIncrements('id');
			$table->unsignedBigInteger('id_docente')->nullable();
			$table->unsignedBigInteger('id_curso')->nullable();
			$table->string('periodo', 100);
			$table->timestamps();

			$table->foreign('id_docente','fk_docente')->references('id')->on('empleados')->onDelete('set null');
			$table->foreign('id_curso','fk_curso_2')->references('id')->on('cursos')->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dictados', function(Blueprint $table){
			$table->dropForeign('fk_docente');
			$table->dropForeign('fk_curso_2');
		});

		Schema::drop('dictados');
	}

}
