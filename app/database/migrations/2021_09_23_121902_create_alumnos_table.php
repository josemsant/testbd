<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumnosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alumnos', function(Blueprint $table){
			$table->bigIncrements('id');
			$table->string('nombre',50);
			$table->timestamps();
		});
		
		DB::statement('ALTER TABLE alumnos AUTO_INCREMENT = 5000;');
		DB::statement('ALTER TABLE alumnos ADD FULLTEXT nombre_alumno(nombre)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('alumnos');
	}

}
