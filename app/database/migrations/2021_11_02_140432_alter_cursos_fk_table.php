<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCursosFkTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cursos', function (Blueprint $table) {
			$table->dropForeign('fk_docente');
			$table->foreign('id_docente', 'fk_docente')->references('id')->on('usuarios')->onDelete('set null');
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cursos', function (Blueprint $table) {
			$table->dropForeign('fk_docente');
			$table->foreign('id_docente', 'fk_docente')->references('id')->on('empleados')->onDelete('set null');
		});
	}
}
