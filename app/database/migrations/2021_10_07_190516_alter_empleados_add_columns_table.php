<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmpleadosAddColumnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('empleados', function(Blueprint $table){
			$table->enum('cargo', array('Responsable', 'Docente', 'Administrativo'))->after('sueldo');
			$table->unsignedBigInteger('id_departamento')->nullable()->after('cargo');

			$table->foreign('id_departamento','id_departamento_2')->references('id')->on('departamentos')->onDelete('set null');
		});

		DB::statement('ALTER TABLE empleados MODIFY telefono varchar(32)');
		DB::statement('ALTER TABLE empleados MODIFY sueldo decimal(10,2)');
		DB::statement('ALTER TABLE empleados MODIFY dni varchar(16)');
		 
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('empleados', function(Blueprint $table){
			$table->dropForeign('id_departamento_2');
			$table->dropColumn('cargo');
			$table->dropColumn('id_departamento');
		});
	}

}
