<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInscripcionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `inscripciones` MODIFY created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL;');
		DB::statement('ALTER TABLE `inscripciones` MODIFY updated_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL;');
		
		Schema::table('inscripciones', function (Blueprint $table) {
			$table->dropColumn('id');			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
