<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSentryUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::rename('users', 'usuarios');
		
		Schema::table('usuarios', function(Blueprint $table){
			$table->string('nombre',50)->after('email');
			$table->string('dni',16)->after('nombre');
			$table->string('telefono',32)->after('dni');
			$table->string('direccion',50)->after('telefono');
			$table->tinyInteger('edad')->after('direccion')->nullable();
			$table->decimal('sueldo',10,2)->after('edad')->nullable();
			$table->enum('cargo', array('Responsable', 'Docente', 'Administrativo'))->after('sueldo')->nullable();
			$table->unsignedBigInteger('id_departamento')->after('cargo')->nullable();
			$table->unsignedBigInteger('id_rol')->after('id_departamento')->nullable();		
			
			$table->foreign('id_departamento','fk_departamento_2')->references('id')->on('departamentos')->onDelete('set null');
			$table->foreign('id_rol','fk_rol')->references('id')->on('roles')->onDelete('set null');
		});

		DB::statement('ALTER TABLE usuarios MODIFY id BIGINT UNSIGNED AUTO_INCREMENT');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usuarios', function(Blueprint $table){
			$table->dropForeign('fk_departamento_2');
			$table->dropForeign('fk_rol');
		});
		Schema::rename('usuarios', 'users');

	}
}
