<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAlumnosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('alumnos', function(Blueprint $table){
			$table->string('email',50)->after('nombre');
			$table->string('telefono',20)->after('email');
			$table->string('direccion',50)->after('telefono');
			$table->tinyInteger('edad')->after('direccion');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('alumnos', function(Blueprint $table){
			$table->dropColumn('email');
			$table->dropColumn('telefono');
			$table->dropColumn('direccion');
			$table->dropColumn('edad');
		});
	}

}
