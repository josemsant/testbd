<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamiliaresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('familiares', function(Blueprint $table){
			$table->bigIncrements('id');
			$table->string('nombre',50);
			$table->string('tipo',50);
			$table->integer('edad');
			$table->unsignedBigInteger('id_empleado')->nullable();
			$table->timestamps();

			$table->foreign('id_empleado','fk_familiar')->references('id')->on('empleados')->onDelete('set null');
		});

		DB::statement('ALTER TABLE familiares ADD FULLTEXT nombre_familiar(nombre)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('familiares');
	}

}
