<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCursosAddColumnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cursos', function(Blueprint $table){
			$table->enum('periodo', array('1° CUATRIMESTRE', '2° CUATRIMESTRE', 'ANUAL'))->after('puntaje');
			$table->unsignedBigInteger('id_docente')->nullable()->after('periodo');

			$table->foreign('id_docente','fk_docente')->references('id')->on('empleados')->onDelete('set null');
		});

		DB::statement('ALTER TABLE cursos MODIFY duracion TINYINT UNSIGNED');
		DB::statement('ALTER TABLE cursos MODIFY puntaje TINYINT UNSIGNED');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE cursos MODIFY duracion INT');
		DB::statement('ALTER TABLE cursos MODIFY puntaje INT');

		Schema::table('cursos', function(Blueprint $table){
			$table->dropForeign('fk_docente');
			$table->dropColumn('id_docente');
			$table->dropColumn('periodo');
		});
	}

}
