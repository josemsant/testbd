<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFamiliaresAddColumnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('familiares', function(Blueprint $table){
			$table->renameColumn('tipo' , 'parentesco');
		});

		DB::statement('ALTER TABLE familiares MODIFY edad TINYINT UNSIGNED');
		DB::statement('ALTER TABLE familiares MODIFY parentesco ENUM("Conyuge","Hijo","Padre","Madre","Otro")');

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE familiares MODIFY edad INT');
		DB::statement('ALTER TABLE familiares MODIFY parentesco VARCHAR(50)');

		Schema::table('familiares', function(Blueprint $table){
			$table->renameColumn('parentesco' , 'tipo');
		});
	}

}
