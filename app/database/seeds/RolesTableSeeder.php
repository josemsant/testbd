<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class RolesTableSeeder extends Seeder {

	public function run()
	{
		$roles = [
					'Alumno',
					'Empleado',
					'Admin'
		];

		foreach ($roles as $role) 		
		{
			$rol = new Rol;

			$rol->nombre = $role;
			$rol->save();
		}
	}

}