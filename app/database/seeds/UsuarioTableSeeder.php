<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsuarioTableSeeder extends Seeder {

	public function run()
	{
		Sentry::createUser(array(
            'nombre'     => 'user',
            'dni'     => '123456',
            'password'  => '123456',
            'id_rol' => '3',
            'activated' => true,
        ));
	}

}