<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CursosTableSeeder extends Seeder
{

	public function run()
	{

		$faker = Faker::create();

		$cursos = [
			'Desarrolador FullStack',
			'Laravel 4.2 Principiante',
			'Laravel 4.2 Intermedio',
			'Lravel 4.2 Experto',
			'JavaScript de 0 a Experto',
			'Bootstrap 3',
			'Todo sobre sistemas operativos',
			'Diseña tu procesador de PC',
			'Analisis de Algoritmos',
		];

	
		foreach ($cursos as $curso) {
			$departamentos = Departamento::all()->random(1);
			$docentes = Usuario::select('id')
				->where('cargo', 'Docente')
				->get()
				->random(1);

			Curso::create([
				'nombre'            => $curso,
				'precio'            => $faker->randomFloat($nbMaxDecimals = NULL, $min = 1000, $max = 10000),
				'duracion'          => $faker->numberBetween($min = 10, $max = 100),
				'puntaje'           => $faker->numberBetween($min = 1, $max = 10),
				'id_docente'        => $docentes->id,
				'id_departamento'   => $departamentos->id,
			]);
		}
	}
}
