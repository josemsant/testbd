<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class FamiliaresTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		
		$tipos = [
					'Esposa',
					'Esposo',
					'hijo',
					'Padre',
					'Madre'
		];

		Familiar::create(['nombre' => 'Villagrán Santiago', 'tipo' => 'hijo', 'edad' => '15','id_empleado' => '3060']);
		Familiar::create(['nombre' => 'Terán Marta', 'tipo' => 'esposa', 'edad' => '40','id_empleado' => '3060']);

		Familiar::create(['nombre' => 'Zagaglia Pedro', 'tipo' => 'hijo', 'edad' => '3','id_empleado' => '3061']);
		Familiar::create(['nombre' => 'Luis', 'tipo' => 'hijo', 'hijo' => '9','id_empleado' => '3061']);
		Familiar::create(['nombre' => 'Peralta Lorena', 'tipo' => 'esposa', 'edad' => '43','id_empleado' => '3061']);

		Familiar::create(['nombre' => 'Villanueva Maria Celeste', 'tipo' => 'hijo', 'edad' => '15','id_empleado' => '3062']);

		Familiar::create(['nombre' => 'Velez julio', 'tipo' => 'hijo', 'edad' => '3','id_empleado' => '3064']);
		Familiar::create(['nombre' => 'Velez Marcela', 'tipo' => 'hijo', 'edad' => '6','id_empleado' => '3064']);

		Familiar::create(['nombre' => 'Zaccheo Josefina', 'tipo' => 'hijo', 'edad' => '14','id_empleado' => '3065']);
		Familiar::create(['nombre' => 'Zaccheo Martin', 'tipo' => 'hijo', 'edad' => '19','id_empleado' => '3065']);

		foreach(range(1, 20) as $index)
		{
			$empleado = Empleado::all()->random(1); 

			Familiar::create([
				'nombre'      => $faker->name,
				'tipo'        => $tipos[array_rand($tipos)],
				'edad'        => $faker->numberBetween($min = 20, $max = 70),
				'id_empleado' => $empleado->id,
			]);
		}
	}

}