<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class DepartamentosTableSeeder extends Seeder {

	public function run()
	{
		$departamentos = [
					'Dpto de Computacion',
					'Dpto de Sistemas operativos',
					'Dpto de Diseño de redes',
					'Dpto de Matematica aplicada',
					'Dpto de Ing Software',
					'Dpto de Arquitectura y Organizacion de Computadoras',
		];

		foreach ($departamentos as $dpto) 		
		{
			$departamento = new Departamento;

			$departamento->nombre = $dpto;
			$departamento->save();
		}
	}

}