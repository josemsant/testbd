<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AlumnosTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Alumno::create([
				'nombre'   => $faker->name,
				'email'   => $faker->email,
				'telefono' => $faker->numberBetween($min = 100000000000, $max = 900000000000),
				'direccion'=> $faker->streetName,
				'edad'   => $faker->numberBetween($min = 1, $max = 120),
			]);
		}
	}

}