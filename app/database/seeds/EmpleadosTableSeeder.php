<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class EmpleadosTableSeeder extends Seeder 
{
	public function run()
	{
		$faker = Faker::create();

		$cargos = [
			'Responsable',
			'Docente',
			'Administrativo',
];

		foreach(range(1, 10) as $index)
		{
			$departamento = Departamento::all()->random(1);

			Empleado::create([
				'nombre'   => $faker->name,
				'dni'      => $faker->numberBetween($min = 10000000, $max = 100000000),
				'telefono' => $faker->numberBetween($min = 100000000, $max = 900000000),
				'domicilio'=> $faker->streetName,
				'sueldo'   => $faker->randomFloat($nbMaxDecimals = NULL, $min = 10000, $max = 100000),
				'cargo'    => $cargos[array_rand($cargos)],
				'id_departamento' => $departamento->id,
			]);
		}
	}

}