<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call('DepartamentosTableSeeder');
		// $this->call('EmpleadosTableSeeder');
		// $this->call('CursosTableSeeder');
		// $this->call('AlumnosTableSeeder');
		$this->call('RolesTableSeeder');
		$this->call('UsuarioTableSeeder');
	}
}
