<?php

class CustomValidator extends \Illuminate\Validation\Validator
{
    public function validateExistsMultiple($attribute, $value, $parameters)
    {
        $modelo = $parameters[0];
        $columna = $parameters[1];
        $ids = explode(',', $value);

        return $modelo::whereIn($columna, $ids)->count() == count($ids);
    }
}
