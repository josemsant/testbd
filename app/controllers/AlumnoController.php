<?php

class AlumnoController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('Alumno.index');
	}

	public function listaAlumnos()
	{
		$buscador = Input::get('search.value');

		$length = Input::get('length');
		$pagina = (Input::get('start') / $length) + 1;

		Input::merge(['page' => $pagina]);

		$alumnos = Usuario::with('cursosInscriptos');

		if ($buscador) {
			$alumnos = $alumnos->where('nombre', 'LIKE', "%$buscador%");
		}

		$alumnos = $alumnos->where('id_rol', '=', '1')->paginate($length);
		$data = $alumnos->getCollection();

		$response = [
			'recordsTotal' => $alumnos->getTotal(),
			'recordsFiltered' => $alumnos->getTotal(),
			'data' => $data
		];

		return Response::json($response);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('Alumno.modal_crear');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$control = array(
			'nombre' 		=> 'required|max:64|regex:/^[a-z ]+$/i',
			'email' 		=> 'required|email',
			'telefono' 		=> 'required|integer|max:1000000000000',
			'direccion' 	=> 'required|max:64',			
			'edad' 			=> 'required|integer|min:1|max:120',			
		);

		$messages = [
			'nombre.required' 		=> 'El nombre es necesario.',
			'nombre.max' 			=> 'El nombre no debe superar los 64 caracteres.',
			'nombre.regex' 			=> 'El nombre solo puede contener caracteres alfabético.',
			'email.required' 		=> 'El email es necesario.',
			'email.email' 			=> 'El email debe tener formato email.',
			'telefono.required' 	=> 'El telefono es necesario.',
			'telefono.integer' 		=> 'Ingrese solo numeros para el telefono.',
			'telefono.max' 			=> 'El telefono no debe superar 12 digitos.',
			'direccion.required' 	=> 'La direccion es necesaria.',
			'direccion.max' 		=> 'La direccion no debe superar los 64 caracteres.',
			'edad.required' 		=> 'La edad es necesaria.',
			'edad.integer' 			=> 'Ingrese solo numeros para la edad.',
			'edad.min' 				=> 'La edad debe ser mayor o igual a 1.',
			'edad.max' 				=> 'La edad debe ser menor o igual a 120.',
		];

		$validacion = Validator::make(Input::all(), $control, $messages);

		if ($validacion->fails()) {
			return Response::json($validacion->getMessageBag(), 409);
		} else {
			$alumno = new Alumno();
			$alumno->nombre = Input::get('nombre');
			$alumno->email = Input::get('email');
			$alumno->telefono = Input::get('telefono');
			$alumno->direccion = Input::get('direccion');
			$alumno->edad = Input::get('edad');
			$alumno->save();

			return Response::json(['mensaje' => 'Alumno registrado con exito'], 201);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$alumno = Usuario::where('id_rol', '=', '1')->find($id);

		return View::make('Alumno.modal_editar', compact('alumno'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$control = array(
			'nombre' 		=> 'required|max:64|regex:/^[a-z ]+$/i',
			'email' 		=> 'required|email',
			'telefono' 		=> 'required|integer|max:1000000000000',
			'direccion' 	=> 'required|max:64',			
			'edad' 			=> 'required|integer|min:1|max:120',			
		);

		$messages = [
			'nombre.required' 		=> 'El nombre es necesario.',
			'nombre.max' 			=> 'El nombre no debe superar los 64 caracteres.',
			'nombre.regex' 			=> 'El nombre solo puede contener caracteres alfabético.',
			'email.required' 		=> 'El email es necesario.',
			'email.email' 			=> 'El email debe tener formato email.',
			'telefono.required' 	=> 'El telefono es necesario.',
			'telefono.integer' 		=> 'Ingrese solo numeros para el telefono.',
			'telefono.max' 			=> 'El telefono no debe superar 12 digitos.',
			'direccion.required' 	=> 'La direccion es necesaria.',
			'direccion.max' 		=> 'La direccion no debe superar los 64 caracteres.',
			'edad.required' 		=> 'La edad es necesaria.',
			'edad.integer' 			=> 'Ingrese solo numeros para la edad.',
			'edad.min' 				=> 'La edad debe ser mayor o igual a 1.',
			'edad.max' 				=> 'La edad debe ser menor o igual a 120.',
		];

		$validacion = Validator::make(Input::all(), $control, $messages);

		if ($validacion->fails()) {
			return Response::json($validacion->getMessageBag(), 409);
		} else {
			$alumno = Usuario::find($id);
			$alumno->nombre = Input::get('nombre');
			$alumno->email = Input::get('email');
			$alumno->telefono = Input::get('telefono');
			$alumno->direccion = Input::get('direccion');
			$alumno->edad = Input::get('edad');
			$alumno->save();
			return Response::json(['mensaje' => 'Alumno Editado con exito'], 201);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$alumno = Sentry::findUserById($id);
		$alumno->delete();
		return Response::json(['mensaje' => 'Alumno eliminado con exito'], 201);
	}

	public function createInscripcion($id)
	{
		$alumno = Usuario::with('cursosInscriptos')						
						->find($id);

		return View::make('Alumno.modal_inscribir_curso', compact('alumno'));
	}

	public function storeInscripcion($idAlumno)
	{
		Validator::resolver(function ($translator, $data, $rules, $messages) {
			return new CustomValidator($translator, $data, $rules, $messages);
		});

		$control = array(
			'cursos' => 'required|exists_multiple:Curso,id',
		);

		$messages = [
			'cursos.exists_multiple' => 'Uno o mas cursos no existen.',
		];

		$validacion = Validator::make(Input::all(), $control, $messages);

		if ($validacion->fails()) {
			return Response::json($validacion->getMessageBag(), 409);
		} else {
			$alumno = Usuario::find($idAlumno);
			$curso = explode(',', Input::get('cursos'));
			$alumno->cursosInscriptos()->sync($curso);

			return Response::json(['mensaje' => 'Alumno inscripto a cursos con exito'], 201);
		}
	}
}
