<?php

class AuxiliaresController extends \BaseController
{

	public function listaDepartamentos()
	{
		$departamentos = Departamento::select('id', 'nombre as text')
			->get();

		return Response::json($departamentos);
	}

	public function listaDocentes()
	{
		$docentes = Usuario::select('id', 'nombre as text')
			->where('cargo', 'Docente')
			->get();

		return Response::json($docentes);
	}

	public function listaCursos()
	{
		$cursos = Curso::select('id', 'nombre as text')
			->get();

		return Response::json($cursos);
	}

	public function listaRoles()
	{
		$roles = Rol::select('id', 'nombre as text')
			->get();

		return Response::json($roles);
	}
}
