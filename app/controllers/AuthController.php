<?php

class AuthController extends \BaseController
{

	public function login()
	{
		return View::make('Login.login');
	}

	public function postLogin()
	{
		try {

			$control = [
				'usuario'    => 'required',
				'contraseña' => 'required',
			];

			$validacion = Validator::make(Input::all(), $control);

			if ($validacion->fails())
				throw new Cartalyst\Sentry\Users\LoginRequiredException;

			$credenciales = array(
				'dni'    => Input::get('usuario'),
				'password' => Input::get('contraseña'),
			);

			$user = Sentry::authenticate($credenciales, false);

			return Redirect::to('/dashboard');
		} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			return Redirect::to('login')
				->with('mensaje_error', 'Los campo usuario y contesañe son requerido')
				->withInput(Input::except('contraseña'));
		}
		// Falta campo password
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			return Redirect::to('login')
				->with('mensaje_error', 'Tus datos son incorrectos')
				->withInput(Input::except('contraseña'));
		}
		// Usuario inactivo
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
			return Redirect::to('login')
				->with('mensaje_error', 'Tus datos son incorrectos')
				->withInput();
		}
		// Password erroneo
		catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
			return Redirect::to('login')
				->with('mensaje_error', 'Usuario o Contraseña incorrectos!')
				->withInput(Input::except('contraseña'));
		}
		// Usuario no se encuentra
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
			return Redirect::to('login')
				->with('mensaje_error', 'Usuario Inexistente.')
				->withInput();
		}
		// Usuario supendido
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
			return Redirect::to('login')
				->with('mensaje_error', 'El usuario fue suspendido')
				->withInput();
		}
		// Usuario banneado
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
			return Redirect::to('login')
				->with('mensaje_error', 'Usuario bloqueado')
				->withInput();
		} catch (Exception $exception) {
			return Redirect::to('login')
				->with('mensaje_error', $exception->getMessage())
				->withInput(Input::except('contraseña'));
		}
	}

	public function recuperar()
	{
		return View::make('Login.recuperar_contraseña_index');
	}

	public function postRecuperar()
	{
		try {

			// $user = Sentry::findUserByCredentials(array(
			// 	'email' => Input::get('email'),
			// ));
			$user = Sentry::findUserByLogin(Input::get('dni'));
			Log::info($user->email);
	
			$resetCode = $user->getResetPasswordCode();

			Log::info($resetCode);

			$data = array('codigo' => $resetCode);

			Mail::send('Login.email', $data, function ($message) use ($user) {
				$message->from('admin@gmail.com', 'admin');
				$message->to($user->email, $user->nombre)->subject('hola!');
			});

			 return View::make('Login.recuperar_contraseña', compact('resetCode','user'));
			// Now you can send this code to your user via email for example.
		} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
			return Redirect::to('login/recuperar')
				->with('mensaje_error', 'El email no pertenece a un usuario registrado')
				->withInput();
		}
	}

	public function postContraseñaNueva($id){
		try
		{
			$user = Sentry::findUserById($id);
			$codigo = Input::get('codigo');
			$nueva_contra = Input::get('contraseña_1');
		
			if ($user->checkResetPasswordCode($codigo))
			{
				// Attempt to reset the user password
				if ($user->attemptResetPassword($codigo, $nueva_contra))
				{
					echo 'exito';
				}
				else
				{
					echo 'fail';
				}
			}
			else
			{
				echo 'codigo invalidado';
			}
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			return Redirect::to('login/recuperar')
				->with('mensaje_error', 'Usuario inexistente')
				->withInput();
		}
	}
}
