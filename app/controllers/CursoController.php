<?php

class CursoController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('Curso.index');
	}

	public function listaCursos()
	{
		$buscador = Input::get('search.value');

		$length = Input::get('length');
		$pagina = (Input::get('start') / $length) + 1;

		Input::merge(['page' => $pagina]);

		$cursos = Curso::with('departamento')
			->with('docente');
		if ($buscador) {
			$cursos = $cursos->where('nombre', 'LIKE', "%$buscador%");
		}

		$cursos = $cursos->paginate($length);

		$data = [];

		foreach ($cursos as $curso) {
			$item = [
				'id' 			=> $curso->id,
				'nombre' 		=> $curso->nombre,
				'precio' 		=> $curso->precio,
				'duracion' 		=> $curso->duracion,
				'puntaje' 		=> $curso->puntaje,
				'periodo' 		=> $curso->periodo,
				'departamento' 	=> $curso->departamento->nombre,
				'docente' 		=> $curso->docente->nombre,
			];
			$data[] = $item;
		};

		$response = [
			'recordsTotal' => $cursos->getTotal(),
			'recordsFiltered' => $cursos->getTotal(),
			'data' => $data
		];

		return Response::json($response);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('Curso.modal_crear');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		Validator::resolver(function ($translator, $data, $rules, $messages) {
			return new CustomValidator($translator, $data, $rules, $messages);
		});

		$control = [
			'nombre' 		=> 'required|max:64',
			'precio' 		=> 'required|numeric',
			'duracion' 		=> 'required|integer',
			'puntaje' 		=> 'required|integer',
			'periodo' 		=> 'required',
			'docente' 		=> 'required|exists_multiple:Empleado,id',
			'departamento' 	=> 'required|exists_multiple:Departamento,id',
		];

		$messages = [
			'nombre.required' 		=> 'El nombre del curso es necesario.',
			'nombre.max' 			=> 'El nombre no debe superar los 64 caracteres.',
			'precio.required' 		=> 'El precio es necesario.',
			'precio.numeric' 		=> 'Ingrese solo numero para el precio.',
			'duracion.required' 	=> 'La duracion en horas es necesario.',
			'duracion.integer' 		=> 'Ingrese solo numero para la duracion.',
			'puntaje.required' 		=> 'El puntaje es necesario.',
			'puntaje.integer' 		=> 'Ingrese solo numero para el puntaje.',
			'periodo.required' 		=> 'El periodo es necesario.',
			'docente.required' 		=> 'El docente es necesario.',
			'docente.exists_multiple' => 'Uno o mas docentes no existen',
			'departamento.required' => 'El departamento es necesario.',
			'departamento.exists_multiple' => 'Uno o mas departamentos no existen'
		];

		$validacion = Validator::make(Input::all(), $control, $messages);

		if ($validacion->fails()) {
			return Response::json($validacion->getMessageBag(), 409);
		} else {
			$curso = new Curso;
			$curso->nombre = Input::get('nombre');
			$curso->precio = Input::get('precio');
			$curso->duracion = Input::get('duracion');
			$curso->puntaje = Input::get('puntaje');
			$curso->periodo = Input::get('periodo');
			$curso->id_docente = Input::get('docente');
			$curso->id_departamento = Input::get('departamento');
			$curso->save();

			return Response::json(['mensaje' => 'Curso registrado con exito'], 201);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$curso = Curso::with('departamento')
			->with('docente')
			->find($id);

		return View::make('Curso.modal_editar', compact('curso'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		Validator::resolver(function ($translator, $data, $rules, $messages) {
			return new CustomValidator($translator, $data, $rules, $messages);
		});

		$control = [
			'nombre' 		=> 'required|max:64',
			'precio' 		=> 'required|numeric',
			'duracion' 		=> 'required|integer',
			'puntaje' 		=> 'required|integer',
			'periodo' 		=> 'required',
			'docente' 		=> 'required|exists:Empleado,id',
			'departamento' 	=> 'required|exists:Departamento,id',
		];

		$messages = [
			'nombre.required' 		=> 'El nombre del curso es necesario.',
			'nombre.max' 			=> 'El nombre no debe superar los 64 caracteres.',
			'precio.required' 		=> 'El precio es necesario.',
			'precio.numeric' 		=> 'Ingrese solo numero para el precio.',
			'duracion.required' 	=> 'La duracion en horas es necesario.',
			'duracion.integer' 		=> 'Ingrese solo numero para la duracion.',
			'puntaje.required' 		=> 'El puntaje es necesario.',
			'puntaje.integer' 		=> 'Ingrese solo numero para el puntaje.',
			'periodo.required' 		=> 'El periodo es necesario.',
			'docente.required' 		=> 'El docente es necesario.',
			'docente.exists' => 'Uno o mas docentes no existen',
			'departamento.required' => 'El departamento es necesario.',
			'departamento.exists' => 'Uno o mas departamentos no existen'
		];

		$validacion = Validator::make(Input::all(), $control, $messages);

		if ($validacion->fails()) {
			return Response::json($validacion->getMessageBag(), 409);
		} else {
			$curso = Curso::find($id);
			$curso->nombre = Input::get('nombre');
			$curso->precio = Input::get('precio');
			$curso->duracion = Input::get('duracion');
			$curso->puntaje = Input::get('puntaje');
			$curso->periodo = Input::get('periodo');
			$curso->id_docente = Input::get('docente');
			$curso->id_departamento = Input::get('departamento');
			$curso->save();

			return Response::json(['mensaje' => 'Curso editado con exito'], 201);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$curso = Curso::find($id);
		$curso->delete();
		return Response::json(['mensaje' => 'Curso eliminado con exito'], 201);
	}
}
