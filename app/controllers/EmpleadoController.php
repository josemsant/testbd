<?php

class EmpleadoController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('Empleado.index');
	}

	public function listaEmpleados()
	{
		$buscador = Input::get('search.value');

		$length = Input::get('length');
		$pagina = (Input::get('start') / $length) + 1;

		Input::merge(['page' => $pagina]);

		$empleados = Usuario::with('departamento')
			->where('id_rol', '=', '2');

		if ($buscador) {
			$empleados = $empleados->where('nombre', 'LIKE', "%$buscador%")
				->orWhere('dni', 'LIKE', "%$buscador%");
		}

		$empleados = $empleados->paginate($length);

		$data = [];

		foreach ($empleados as $empleado) {

			if (!isset($empleado->id_departamento)) {
				$departamento = "Sin departamento";
			} else {
				$departamento = $empleado->departamento->nombre;
			}

			$item = [
				'id' => $empleado->id,
				'nombre' => $empleado->nombre,
				'direccion' => $empleado->direccion,
				'telefono' => $empleado->telefono,
				'dni' => $empleado->dni,
				'sueldo' => $empleado->sueldo,
				'cargo' => $empleado->cargo,
				'departamento' => $departamento
			];

			$data[] = $item;
		};

		$response = [
			'recordsTotal' => $empleados->getTotal(),
			'recordsFiltered' => $empleados->getTotal(),
			'data' => $data
		];

		return Response::json($response);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('Empleado.modal_crear');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		Validator::resolver(function ($translator, $data, $rules, $messages) {
			return new CustomValidator($translator, $data, $rules, $messages);
		});

		$control = [
			'nombre' 		=> 'required|max:64|regex:/^[a-z ]+$/i',
			'domicilio' 	=> 'required|max:64',
			'telefono' 		=> 'required|numeric|max:1000000000000',
			'dni' 			=> 'required|numeric',
			'sueldo' 		=> 'required|numeric',
			'cargo' 		=> 'required',
			'departamento' 	=> 'required|exists:Departamento,id',
		];

		$messages = [
			'nombre.required' 		=> 'El nombre es necesario.',
			'nombre.max' 			=> 'El nombre no debe superar los 64 caracteres.',
			'nombre.regex' 			=> 'El nombre solo puede contener caracteres alfabético.',
			'domicilio.required' 	=> 'El domicilio es necesario.',
			'domicilio.max' 		=> 'El domicilio no debe superar los 64 caracteres.',
			'telefono.required' 	=> 'El telefono es necesario.',
			'telefono.numeric' 		=> 'Ingrese solo numero para el telefono.',
			'telefono.max' 			=> 'El telefono no debe superar 12 digitos.',
			'dni.required' 			=> 'El dni es necesario.',
			'dni.numeric' 			=> 'Ingrese solo numero para el dni.',
			'sueldo.required' 		=> 'El sueldo es necesario.',
			'sueldo.numeric' 		=> 'Ingrese solo numero para el sueldo.',
			'cargo.required' 		=> 'El cargo es necesario.',
			'departamento.required' => 'El departamento es necesario.',
			'departamento.exists' 	=> 'Uno o mas departamentos no existen'
		];

		$validacion = Validator::make(Input::all(), $control, $messages);

		if ($validacion->fails()) {
			return Response::json($validacion->getMessageBag(), 409);
		} else {
			$empleado = new Empleado;
			$empleado->nombre = Input::get('nombre');
			$empleado->domicilio = Input::get('domicilio');
			$empleado->telefono = Input::get('telefono');
			$empleado->dni = Input::get('dni');
			$empleado->sueldo = Input::get('sueldo');
			$empleado->cargo = Input::get('cargo');
			$empleado->id_departamento = Input::get('departamento');
			$empleado->save();

			return Response::json(['mensaje' => 'Empleado registrado con exito'], 201);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// $empleado = Usuario::with('cursos')
		// 				->with('departamento')
		// 				->find(2);

		// return View::make('Empleado.info_empleado', compact('empleado'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$empleado = Usuario::with('departamento')
			->where('id_rol', '=', '2')
			->find($id);

		return View::make('Empleado.modal_editar', compact('empleado'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		Validator::resolver(function ($translator, $data, $rules, $messages) {
			return new CustomValidator($translator, $data, $rules, $messages);
		});

		$control = [
			'nombre' 		=> 'required|max:64|regex:/^[a-z ]+$/i',
			'direccion' 	=> 'required|max:64',
			'telefono' 		=> 'required|integer|max:1000000000000',
			'dni' 			=> 'required|integer',
			'sueldo' 		=> 'required|numeric',
			'cargo' 		=> 'required',
			'departamento' 	=> 'required|exists_multiple:Departamento,id',
		];

		$messages = [
			'nombre.required' 		=> 'El nombre es necesario.',
			'nombre.max' 			=> 'El nombre no debe superar los 64 caracteres.',
			'nombre.regex' 			=> 'El nombre solo puede contener caracteres alfabético.',
			'direccion.required' 	=> 'El domicilio es necesario.',
			'direccion.max' 		=> 'El domicilio no debe superar los 64 caracteres.',
			'telefono.required' 	=> 'El telefono es necesario.',
			'telefono.integer' 		=> 'Ingrese solo numero para el telefono.',
			'telefono.max' 			=> 'El telefono no debe superar 12 digitos.',
			'dni.required' 			=> 'El dni es necesario.',
			'dni.integer' 			=> 'Ingrese solo numero para el dni.',
			'sueldo.required' 		=> 'El sueldo es necesario.',
			'sueldo.numeric' 		=> 'Ingrese solo numero para el sueldo.',
			'cargo.required' 		=> 'El cargo es necesario.',
			'departamento.required' => 'El departamento es necesario.',
			'departamento.exists_multiple' => 'Uno o mas departamentos no existen'
		];

		$validacion = Validator::make(Input::all(), $control, $messages);

		if ($validacion->fails()) {
			return Response::json($validacion->getMessageBag(), 409);
		} else {
			$empleado = Usuario::find($id);
			$empleado->nombre = Input::get('nombre');
			$empleado->direccion = Input::get('direccion');
			$empleado->telefono = Input::get('telefono');
			$empleado->dni = Input::get('dni');
			$empleado->sueldo = Input::get('sueldo');
			$empleado->cargo = Input::get('cargo');
			$empleado->id_departamento = Input::get('departamento');
			$empleado->save();

			return Response::json(['mensaje' => 'Empleado editado con exito'], 201);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$empleado = Sentry::findUserById($id);
		$empleado->delete();
		return Response::json(['mensaje' => 'Empleado eliminado con exito'], 201);
	}
}
