<?php

class UsuarioController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('Usuario.index');
	}

	public function listaUsuarios()
	{
		$buscador = Input::get('search.value');

		$length = Input::get('length');
		$pagina = (Input::get('start') / $length) + 1;

		Input::merge(['page' => $pagina]);

		$usuarios = Usuario::with('rol');

		if ($buscador) {
			$usuarios = $usuarios->where('nombre', 'LIKE', "%$buscador%");
		}

		$usuarios = $usuarios->paginate($length);

		$data = $usuarios->getCollection();

		$response = [
			'recordsTotal' => $usuarios->getTotal(),
			'recordsFiltered' => $usuarios->getTotal(),
			'data' => $data
		];

		return Response::json($response);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	public function createAlumno()
	{
		return View::make('Usuario.modal_crear_usuario_alumno');
	}

	public function createEmpleado()
	{
		return View::make('Usuario.modal_crear_usuario_empleado');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storeAlumno()
	{
		$control = array(
			'nombre' 	=> 'required|max:64|regex:/^[a-z ]+$/i',
			'email' 	=> 'required|email|unique:usuarios',
			'telefono' 	=> 'required|integer|max:1000000000000',
			'direccion' => 'required|max:64',
			'edad' 		=> 'required|integer|min:1|max:120',
			'dni'		=> 'required|integer|unique:usuarios',
			'contraseña_1'	=> 'required'
		);

		$messages = [
			'nombre.required' 		=> 'El nombre es necesario.',
			'nombre.max' 			=> 'El nombre no debe superar los 64 caracteres.',
			'nombre.regex' 			=> 'El nombre solo puede contener caracteres alfabético.',
			'email.required' 		=> 'El email es necesario.',
			'email.email' 			=> 'El email debe tener formato email.',
			'email.unique' 			=> 'El email ingresado ya se registro, ingrese otro por favor.',
			'telefono.required' 	=> 'El telefono es necesario.',
			'telefono.integer' 		=> 'Ingrese solo numeros para el telefono.',
			'telefono.max' 			=> 'El telefono no debe superar 12 digitos.',
			'direccion.required' 	=> 'La direccion es necesaria.',
			'direccion.max' 		=> 'La direccion no debe superar los 64 caracteres.',
			'edad.required' 		=> 'La edad es necesaria.',
			'edad.integer' 			=> 'Ingrese solo numeros para la edad.',
			'edad.min' 				=> 'La edad debe ser mayor o igual a 1.',
			'edad.max' 				=> 'La edad debe ser menor o igual a 120.',
			'dni.unique' 			=> 'El DNI ingresado ya se registro, ingrese otro por favor',
			'dni.required' 			=> 'El dni es necesaria.',
			'dni.integer' 			=> 'Ingrese solo numeros para el dni.',
			'contraseña_1.required' => 'La contraseña es necesaria es necesaria.',
		];

		$validacion = Validator::make(Input::all(), $control, $messages);

		if ($validacion->fails()) {
			return Response::json($validacion->getMessageBag(), 409);
		} else {
			$usuario = new Usuario();
			$usuario->nombre = Input::get('nombre');
			$usuario->email = Input::get('email');
			$usuario->password = Input::get('contraseña_1');
			$usuario->telefono = Input::get('telefono');
			$usuario->direccion = Input::get('direccion');
			$usuario->edad = Input::get('edad');
			$usuario->dni = Input::get('dni');
			$usuario->sueldo = null;
			$usuario->cargo = null;
			$usuario->id_departamento = null;
			$usuario->activated = true;
			$usuario->id_rol = 1;
			$usuario->save();

			return Response::json(['mensaje' => 'Alumno registrado con exito'], 201);
		}
	}

	public function storeEmpleado()
	{
		$control = array(
			'nombre' 		=> 'required|max:64|regex:/^[a-z ]+$/i',
			'email' 		=> 'required|email|unique:usuarios',
			'telefono' 		=> 'required|integer|max:1000000000000',
			'direccion' 	=> 'required|max:64',
			'edad' 			=> 'required|integer|min:1|max:120',
			'dni'			=> 'required|integer|unique:usuarios',
			'contraseña_1'	=> 'required',
			'sueldo' 		=> 'required|numeric',
			'cargo' 		=> 'required',
			'departamento' 	=> 'required|exists:departamentos,id',
		);

		$messages = [
			'nombre.required' 		=> 'El nombre es necesario.',
			'nombre.max' 			=> 'El nombre no debe superar los 64 caracteres.',
			'nombre.regex' 			=> 'El nombre solo puede contener caracteres alfabético.',
			'email.required' 		=> 'El email es necesario.',
			'email.email' 			=> 'El email debe tener formato email.',
			'email.unique' 			=> 'El email ingresado ya se registro, ingrese otro por favor.',
			'telefono.required' 	=> 'El telefono es necesario.',
			'telefono.integer' 		=> 'Ingrese solo numeros para el telefono.',
			'telefono.max' 			=> 'El telefono no debe superar 12 digitos.',
			'direccion.required' 	=> 'La direccion es necesaria.',
			'direccion.max' 		=> 'La direccion no debe superar los 64 caracteres.',
			'edad.required' 		=> 'La edad es necesaria.',
			'edad.integer' 			=> 'Ingrese solo numeros para la edad.',
			'edad.min' 				=> 'La edad debe ser mayor o igual a 1.',
			'edad.max' 				=> 'La edad debe ser menor o igual a 120.',
			'dni.required' 			=> 'El dni es necesaria.',
			'dni.integer' 			=> 'Ingrese solo numeros para el dni.',
			'dni.unique' 			=> 'El dni ingresado ya se registro, ingrese otro por favor.',
			'contraseña_1.required' => 'La contraseña es necesaria es necesaria.',
			'sueldo.required' 		=> 'El sueldo es necesario.',
			'sueldo.numeric' 		=> 'Ingrese solo numero para el sueldo.',
			'cargo.required' 		=> 'El cargo es necesario.',
			'departamento.required' => 'El departamento es necesario.',
			'departamento.exists' 	=> 'Uno o mas departamentos no existen'
		];

		$validacion = Validator::make(Input::all(), $control, $messages);

		if ($validacion->fails()) {
			return Response::json($validacion->getMessageBag(), 409);
		} else {

			// Sentry::createUser(array(
			// 	'dni'     => Input::get('dni'),
			// 	'password'  => Input::get('contraseña_1'),
			// 	'id_rol' => '2',
			// 	'activated' => true,
			// ));

			$usuario = new Usuario();
			$usuario->nombre = Input::get('nombre');
			$usuario->email = Input::get('email');
			$usuario->password = Hash::make(Input::get('contraseña_1'));
			$usuario->telefono = Input::get('telefono');
			$usuario->direccion = Input::get('direccion');
			$usuario->edad = Input::get('edad');
			$usuario->dni = Input::get('dni');
			$usuario->sueldo = Input::get('sueldo');
			$usuario->cargo = Input::get('cargo');
			$usuario->id_departamento = Input::get('departamento');
			$usuario->id_rol = 2;
			$usuario->activated = true;
			$usuario->save();

			return Response::json(['mensaje' => 'Empleado registrado con exito'], 201);
		}
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	
}
