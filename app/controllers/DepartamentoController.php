<?php

class DepartamentoController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('Departamento.index');
	}

	public function listaDepartamentos()
	{
		$buscador = Input::get('search.value');

		$length = Input::get('length');
		$pagina = (Input::get('start') / $length) + 1;

		Input::merge(['page' => $pagina]);

		$departamentos = Departamento::paginate($length);

		if ($buscador) {
			$departamentos = Departamento::where('nombre', 'LIKE', "%$buscador%")
				->paginate($length);
		}

		$data = $departamentos->getCollection();

		$response = [
			'recordsTotal' => $departamentos->getTotal(),
			'recordsFiltered' => $departamentos->getTotal(),
			'data' => $data
		];

		return Response::json($response);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('Departamento.modal_crear');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$control = array(
			'nombre' => 'required|alpha'
		);

		$messages = [
			'nombre.required' => 'El nombre del departamento es necesario.',
			'nombre.alpha'=> 'El nombre solo puede contener caracteres alfabético.'
		];

		$validacion = Validator::make(Input::all(), $control, $messages);

		if ($validacion->fails()) {
			return Response::json($validacion->getMessageBag(), 409);
		} else {
			$departamento = new Departamento;
			$departamento->nombre = Input::get('nombre');
			$departamento->save();

			return Response::json(['mensaje' => 'Departamento registrado con exito'], 201);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$departamento = Departamento::find($id);
		return View::make('Departamento.modal_editar', compact('departamento'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$control = [
			'nombre' => 'required|regex:/^[a-z ]+$/i'			
		];

		$messages = [
			'nombre.required' => 'El nombre del departamento es necesario.',
			'nombre.regex'=> 'El nombre solo puede contener caracteres alfabético.'
		];

		$validacion = Validator::make(Input::all(), $control, $messages);

		if ($validacion->fails()) {
			return Response::json($validacion->getMessageBag(), 409);
		} else {
			$departamento = Departamento::find($id);
			$departamento->nombre = Input::get('nombre');
			$departamento->save();
			return Response::json(['mensaje' => 'Departamento editado con exito'], 201);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$departamento = Departamento::find($id);
		$departamento->delete();
		return Response::json(['mensaje' => 'Departamento eliminado con exito'], 201);
	}
}
