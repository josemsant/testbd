<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
class testController extends BaseController {

	//1.- saber el nombre, costo o precio, duración en horas y puntaje de un curso
	public function pt1(){
		$cursos = Curso::select('nombre','precio','duracion','puntaje')
						->get();

		return Response::json($cursos);
	}

	//2.- quiero saber el nombre de los responsables de cada departamento 
	public function pt2(){
		$departamentos = Departamento::with(['empleados' => function($q){
			$q->where('cargo' , 'Responsable');
		}])->get();

		$data = [];

		foreach ($departamentos as $departamento) {
			$item = [
				'departamento' => $departamento->nombre,
				'responsable' => $departamento->empleados[0]->nombre
			];
			
			$data[] = $item;
		}
		return Response::json($data);
	}
	//191277998
	//Quien dicta cada curso, en que cuatrimestre del año y a que departamento depende cada curso
	public function pt3(){		
		$cursos = Curso::with('dictados.docente')
						->with('departamento')
						->get();
		$data = [];

		foreach ($cursos as $curso ) {

			$docente = array_column($curso->dictados->toArray(), 'docente');
			$docente = array_column($docente, 'nombre');
			
			$dictados = [];

			foreach ($curso->dictados as $dictado) {

				$item_2 = [
					'Docente' => $dictado->docente->nombre,
					'Peridodo' => $dictado->periodo
				];

				$dictados[] = $item_2;
			}

			$item = [
				'Departamento' => $curso->departamento->nombre,
				'Curso' => $curso->nombre,
				'Dictados' => $dictados
			];
			
			$data[] = $item;
		}

		return Response::json($data);
	}

	// Quien es el responsable de cada departamento y cual el que tiene el sueldo más alto
	public function pt4(){
		$departamentos = Departamento::with(['empleados' => function($q){
			$q->where('cargo' , 'Responsable');
		}])->get();

		$data = [];

		foreach ($departamentos as $departamento) {

			$responsables = [];

			foreach ($departamento->empleados as $empleado) {

				$item_empleado = [
					'nombre' => $empleado->nombre,
					'sueldo' => $empleado->sueldo
				];

				$responsables[] = $item_empleado;
			}
			$item = [
				'departamento' => $departamento->nombre,
				'responsable' => $responsables
			];
			
			$data[] = $item;
		}
		return Response::json($data);
	}

	// 5.- También se desea saber el sueldo de cada empleado y la información particular 
	//(dni, teléfono, domicilio)*
	public function pt5(){
		$responsables = Empleado::select('sueldo', 'dni', 'telefono', 'domicilio')
								->has('departamentos')
								->orderBy('sueldo','desc')
								->get();

		return Response::json($responsables);

	}

	// 6.- Cuántos empleados tienen dos o más hijos
	public function pt6(){		
		$empleados = Empleado::whereHas('familiares', function ($query){
								$query->where('tipo','hijo')
									->groupBy('tipo')
									->having(DB::raw('count(*)'), '>=', 2)
									->orderBy(DB::raw('count(tipo)'),'desc');
							})
							->get();

		$cantidad = $empleados->count();

		return Response::json($cantidad);
	}

	// 7.- Que empleado tiene la mayor cantidad
	public function pt7(){
		$empleados = Empleado::select('empleados.*')
			->join('familiares','empleados.id', '=', 'familiares.id_empleado')
			->where('tipo','hijo')
			->groupBy('familiares.id_empleado')
			->orderByRaw('COUNT(*) DESC')
			->first();

		return Response::json($empleados);
	}

	//9.- Realice la inscripción de los alumnos 5005, 5031, 5068 en todos 
	//los cursos de que dependan del departamento de computación, se dicten 
	//en el segundo cuatrimestre y den 5 puntos
	public function pt9(){
		$cursos = Curso::whereHas('departamento' , function ($q) {
			$q->where('nombre', 'Dpto de computacion');
		})
		->where('puntaje' , '=' , 5)
		->whereHas('dictados' , function ($q) {
			$q->where('periodo', '2° CUAT');
		})
		->get();

		return Response::json($cursos);

		$alumnos = ['5005','5031','5068'];

		if(count($cursos) != 0){
			foreach($cursos as $curso){
				foreach($alumnos as $alumno){
					$inscripcione = new Inscripcion;
					$inscripcione->id_curso = $curso->id;
					$inscripcione->id_alumno = $alumno;
					$inscripcione->save();
				}
			}
			return "Alumnos inscriptos con Exito";
		};
		return "No se puedo inscribir los alumnos";		
	}

	// 12.- Modifique todos los precios de todos los cursos anuales en un 5%, los que se dictan en el 
	// segundo cuatrimestre en 8% y los que se dictan en el primer cuatrimestre en un 6,5%
	public function pt12(){		
				
		
		return "discutir sobre el periodo en tabla curso";
	}
}
