<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('logout', function () {
	Sentry::logout();

	return Redirect::to('/');
});

Route::get('/login', 'AuthController@login');
Route::post('/login', 'AuthController@postLogin');
Route::get('/login/recuperar', 'AuthController@recuperar');
Route::post('/login/recuperar', 'AuthController@postRecuperar');
Route::post('/login/contraseña/{id}', 'AuthController@postContraseñaNueva');

Route::get('/', function (){
	// $rol = Rol::where('nombre', '=', 'Admin')->first();
	// $rol = Rol::select('id')->where('nombre', '=', 'Admin')->get();
	if (Sentry::check()){
		return Redirect::to('/dashboard');
	}
	return Redirect::to('/login');
	// return $rol->id;
});

Route::get('empleados_info/{id}', function($id){
	if(Sentry::check()){
		$empleado = Usuario::with('cursos')
						->with('departamento')
						->find($id);
		// return $empleado;
		return View::make('Empleado.info_empleado', compact('empleado'));
	}
	return Redirect::to('/login');
});

Route::get('/alumnos_info/{id}', function($id){
	if(Sentry::check()){
		$alumno = Usuario::with('cursosInscriptos')					
						->find($id);
		// return $alumno;
		return View::make('Alumno.info_alumno', compact('alumno'));
	}
	return Redirect::to('/login');
});

// Route::resource('/empleados_info/{id}', 'EmpleadoController@show');

Route::group(array('before' => 'auth.admin' ), function () {
	Route::get('/dashboard', function () {
		return View::make('layouts.master');
	});

	Route::get('/lista_usuarios', 'UsuarioController@listaUsuarios');
	Route::get('/lista_usuarios_roles', 'auxiliaresController@listaRoles');
	Route::get('/usuarios/create_alumnos', 'UsuarioController@createAlumno');
	Route::post('/usuarios/store_alumnos', 'UsuarioController@storeAlumno');
	Route::get('/usuarios/create_empleados', 'UsuarioController@createEmpleado');
	Route::post('/usuarios/store_empleados', 'UsuarioController@storeEmpleado');
	Route::resource('usuarios', 'UsuarioController');

	Route::get('/lista_departamentos', 'DepartamentoController@listaDepartamentos');
	Route::resource('departamentos', 'DepartamentoController');
	
	Route::get('/lista_empleados', 'EmpleadoController@listaEmpleados');
	Route::get('/lista_empleados_departamentos', 'auxiliaresController@listaDepartamentos');
	Route::resource('empleados', 'EmpleadoController');	

	Route::get('/lista_cursos', 'CursoController@listaCursos');
	Route::get('/lista_cursos_departamentos', 'auxiliaresController@listaDepartamentos');
	Route::get('/lista_cursos_docentes', 'auxiliaresController@listaDocentes');
	Route::resource('cursos', 'CursoController');

	Route::get('/lista_alumnos', 'AlumnoController@listaAlumnos');
	Route::get('/lista_alumnos_cursos', 'auxiliaresController@listaCursos');
	Route::get('/alumnos_inscribir/{id}', 'AlumnoController@createInscripcion');
	Route::post('/alumnos/{id}/inscripcion', 'AlumnoController@storeInscripcion');
	Route::resource('alumnos', 'AlumnoController');
});

// Route::get('/pt1', 'testController@pt1');
// Route::get('/pt2', 'testController@pt2');
// Route::get('/pt3', 'testController@pt3');
// Route::get('/pt4', 'testController@pt4');
// Route::get('/pt5', 'testController@pt5');
// Route::get('/pt6', 'testController@pt6');
// Route::get('/pt7', 'testController@pt7');
// Route::get('/pt9', 'testController@pt9');
// Route::get('/pt12', 'testController@pt12');
