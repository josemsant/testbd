<?php

class Curso extends Eloquent
{

    protected $table = 'cursos';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'precio', 'duracion', 'puntaje', 'periodo', 'id_departamento', 'id_docente'];

    public function departamento()
    {
        return $this->belongsTo(Departamento::class , 'id_departamento' , 'id');
    }

    // public function docente()
    // {
    //     return $this->belongsTo(Empleado::class, 'id_docente','id');
    // }

    public function docente()
    {
        return $this->belongsTo(Usuario::class, 'id_docente','id');
    }

    public function alumnos()
    {
        return $this->belongsToMany(Usuario::class , 'inscripciones' , 'id_curso' , 'id_alumno');
    }
    

}