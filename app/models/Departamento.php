<?php

class Departamento extends Eloquent
{

    protected $table = 'departamentos';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'id_responsable'];

    public function cursos()
    {
        return $this->hasMany(Curso::class, 'id_departamento' , 'id');
    }

    public function empleados()
    {
        return $this->hasMany(Usuario::class , 'id_departamento' , 'id');
    }
    

}