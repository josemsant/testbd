<?php

class Familiar extends Eloquent
{

    protected $table = 'familiares';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'tipo', 'edad', 'id_empleado'];

    public function empleado()
    {
        return $this->belongsTo(Empleado::class, 'id_empleado' , 'id');
    }

}