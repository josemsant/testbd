<?php

class Usuario extends Cartalyst\Sentry\Users\Eloquent\User
{

    protected $table = 'usuarios';

    protected $primaryKey = 'id';

    public function rol()
    {
        return $this->belongsTo(Rol::class, 'id_rol', 'id');
    }    

    public function departamento()
    {
        return $this->belongsTo(Departamento::class, 'id_departamento' , 'id');
    }

    public function cursos()
    {
        return $this->hasMany(Curso::class, 'id_docente' , 'id');
    }

    public function cursosInscriptos()
    {
        return $this->belongsToMany(Curso::class , 'inscripciones' , 'id_alumno' , 'id_curso');
    }
}