<?php

class Alumno extends Eloquent
{

    protected $table = 'alumnos';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'email', 'telefono', 'direccion', 'edad'];

    public function cursos()
    {
        return $this->belongsToMany(Curso::class , 'inscripciones' , 'id_alumno' , 'id_curso');
    }

}