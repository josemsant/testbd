<?php

class Dictado extends Eloquent 
{

    protected $table = 'dictados';

    protected $primaryKey = 'id';

    protected $fillable = ['id_docente' , 'id_curso' , 'periodo' , 'precio'];
   
    public function cursos()
    {
        return $this->hasMany(Curso::class, 'id', 'id_curso');
    }

    public function docente()
    {
        return $this->hasOne(Empleado::class, 'id', 'id_docente');
    }
}