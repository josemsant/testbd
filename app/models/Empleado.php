<?php

class Empleado extends Eloquent 
{

    protected $table = 'empleados';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre' , 'dni' , 'telefono' , 'domicilio' , 'sueldo' , 'cargo' , 'id_departamento'];


    public function departamento()
    {
        return $this->belongsTo(Departamento::class, 'id_departamento' , 'id');
    }

    public function cursos()
    {
        return $this->hasMany(Curso::class, 'id_docente' , 'id');
    }

}