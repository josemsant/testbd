<?php

class Rol extends Eloquent
{

    protected $table = 'roles';

    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $fillable = ['nombre'];

    public function usuarios()
    {
        return $this->hasMany(Curso::class);
    }    

}