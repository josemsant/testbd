<script>
    function remoteModal(url, id_modal) {
        var modal = null;

        $.ajax({
            url: url,
            type: 'get',
            dataType: 'html',
            success: function(data) {
                modal = $('<div class="modal fade" id="' + id_modal + '"></div>');
                modal.html(data);
                modal.modal({
                    backdrop: 'static',
                    keyboard: false
                });

                modal.find('[data-dismiss="modal"]').click(function() {
                    setTimeout(function() {
                        modal.modal('hide');
                        modal.remove();
                    }, 600);
                });
            },
            error: function(jqXHR) {
                //
            }
        });
    }

    function listaDepartamentos(idSelect) {
        $.ajax({
            url: '/lista_empleados_departamentos',
            method: 'GET',
            success: function(departamentos) {
                $(idSelect).select2({
                    data: departamentos,
                    placeholder: "Selecionar Departamento",
                    allowClear: true
                });
            }
        })
    };

    function listaDocentes(idSelect) {
        $.ajax({
            url: '/lista_cursos_docentes',
            method: 'GET',
            success: function(departamentos) {
                $(idSelect).select2({
                    data: departamentos,
                    placeholder: "Selecionar Docente",
                    allowClear: true
                });
            }
        });
    };

    function listaCursos(idSelect) {
        $.ajax({
            url: '/lista_alumnos_cursos',
            method: 'GET',
            success: function(cursos) {
                $(idSelect).select2({
                    data: cursos,
                    placeholder: "Selecionar Curso",
                    multiple: true,
                });
            }
        });
    };

    function listaRoles(idSelect) {
        $.ajax({
            url: '/lista_usuarios_roles',
            method: 'GET',
            success: function(roles) {
                $(idSelect).select2({
                    data: roles,
                    placeholder: "Selecionar Rol",
                    allowClear: true
                });
            }
        })
    };

    var Cargos = [{
            id: "Responsable",
            text: "Responsable"
        },
        {
            id: "Docente",
            text: "Docente"
        },
        {
            id: "Administrativo",
            text: "Administrativo"
        }
    ];

    function listaCargos(idSelect) {
        $(idSelect).select2({
            data: Cargos,
            placeholder: "Selecionar Cargo",
            allowClear: true
        });
    };

    var Periodos = [{
            id: "1° CUATRIMESTRE",
            text: "1° CUATRIMESTRE"
        },
        {
            id: "2° CUATRIMESTRE",
            text: "2° CUATRIMESTRE"
        },
        {
            id: "ANUAL",
            text: "ANUAL"
        }
    ];

    function listaPeriodos(idSelect) {
        $(idSelect).select2({
            data: Periodos,
            placeholder: "Selecionar Periodo",
            allowClear: true
        });
    };
</script>