<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap Recursos -->
    {{HTML::style('bootstrap/css/bootstrap.min.css')}}    
    <!-- End Bootstrap Recursos -->

    <!-- FontAwesome Recursos -->
    {{HTML::style('FontAwesome/css/all.min.css')}}
    <!-- End FontAwesome Recursos -->

    <!-- Select 2 Recursos -->
    {{HTML::style('Select2/select2.css')}}
    {{HTML::style('Select2/select2-bootstrap.css')}}
    <!-- End Select 2 Recursos -->

    <!-- Admin Dash Recursos -->
    {{HTML::style('admin/favicon.ico')}}
    {{HTML::style('admin/css/dashboard.css')}}
    <!-- End Admin Dash Recursos -->

    <!-- Datatable Recursos -->
    {{HTML::style('DataTables/css/dataTables.bootstrap.min.css')}}
    <!-- {{HTML::style('DataTables/css/jquery.dataTables.min.css')}} -->
    <!-- End Bootstrap Recursos -->

    <!-- Estilos Personalizados Recursos -->
    {{HTML::style('css/estilos.css')}}
    <!-- End Estilos Personalizados Recursos -->
</head>

<body>
    <!-- Menu -->
    @include('layouts.nav')
    <!-- End Menu -->
    <!-- Contenedor  -->
    <div class="container-fluid">
        <div class="row">
            <!-- Sidebar -->
            @include('layouts.sidebar')
            <!-- End Sidebar -->
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1 class="page-header">@yield('title page')</h1>
                <!-- <h2 class="sub-header">Section title</h2> -->
                <!-- Table -->
                @yield('content')
                <!-- End Table -->
            </div>
        </div>
    </div>
    <!-- End Contenedor  -->
    <!-- scripts -->
    {{HTML::script('jquery/jquery-2.1.3.js')}}

    <!-- jQuery Validation -->
    {{HTML::script('jquery-validation/dist/jquery.validate.min.js')}}

    <!-- BootStrap 3 -->
    {{HTML::script('bootstrap/js/bootstrap.min.js')}}

    <!-- SweetAlert2 -->
    {{HTML::script('SweelAlert2/sweetalert2.all.min.js')}}

    <!-- DataTable -->
    {{HTML::script('DataTables/js/jquery.dataTables.min.js')}}
    {{HTML::script('DataTables/js/dataTables.bootstrap.min.js')}}

    <!-- Select 2 -->
    {{HTML::script('Select2/select2.min.js')}}

    @include('layouts.script')

    @yield('script_vista')
    <!--End scripts -->
</body>

</html>