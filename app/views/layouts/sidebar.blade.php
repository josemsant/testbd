<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
        <li class="active"><a href="/">Inicio</a></li>
        <li><a href="/departamentos">Departamentos</a></li>
        <li><a href="/empleados">Empleados</a></li>
        <li><a href="/cursos">Cursos</a></li>
        <li><a href="/alumnos">Alumnos</a></li>
        <li><a href="/usuarios">Usuarios</a></li>
    </ul>
</div>