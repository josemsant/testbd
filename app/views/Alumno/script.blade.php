<script>
    var tablaAlumnos = $('#tablaAlumnos');

    $(document).ready(function() {
        tablaAlumnos.DataTable({
            "processing": true,
            "serverSide": true,
            'pageLength': 5,
            "dom": 'tip',
            'ajax': {
                url: '/lista_alumnos',
                method: 'GET'
            },
            "language": {
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "Search:": "Buscar:",
                "processing": "Cargando...",
                "emptyTable": "No hay datos disponibles",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "infoEmpty": "Mostrando 0 de 0 de 0 registros",
                "infoFiltered": "(fitrado de _MAX_ registros totales)",
                "zeroRecords": "No se encontraron coincidencias",
                "infoThousands": ",",
                "lengthMenu": "Mostrar _MENU_ registros",
            },
            'columnDefs': [{
                    targets: [0],
                    data: 'id',
                },
                {
                    bSearchable: true,
                    targets: [1],
                    data: 'nombre'
                },
                {
                    targets: [2],
                    data: 'email'
                },
                {
                    targets: [3],
                    data: 'telefono'
                },
                {
                    targets: [4],
                    data: 'direccion'
                },
                {
                    targets: [5],
                    data: 'edad'
                },
                {
                    targets: [6],
                    data: 'cursos_inscriptos',
                    mRender: function(data, type, row) {
                        let etiqueta = $('<ul style="list-style: none;"></ul>');
                        if (data.length === 0) {
                            etiqueta.append($('<li> Sin Cursos </li>'));
                        } else {
                            data.forEach(function(curso) {
                                etiqueta.append($('<li>' + curso.nombre + '</li>'));
                            });
                        }
                        return etiqueta.html();
                    }
                },
                {
                    data: null,
                    targets: [7],
                    mRender: function(data, type, fill) {
                        return "<div class='btn-group'>" +
                            "<a class='btn btn-default dropdown-toggle' data-toggle='dropdown' href='#'>" +
                            "<i class='fa fa-cog'></i> Acciones" +
                            "</a>" +
                            "<ul class='dropdown-menu pull-right'>" +
                            "<li>" +
                            '<button type="button" class="btn btn-warning" onclick="formEditarAlumno(' + data.id + ')">' +
                            '<i class="fa fa-edit"></i> Editar' +
                            '</button>' +
                            "</li>" +
                            "<li>" +
                            '<button type="button" class="btn btn-danger" onclick="eliminarAlumno(' + data.id + ')">' +
                            '<i class="fa fa-trash"></i> Borrar' +
                            '</button>' +
                            "</li>" +
                            "<li>" +
                            '<button type="button" class="btn btn-success" onclick="formCrearInscripcion(' + data.id + ')">' +
                            '<i class="fa fa-trash"></i> Inscribir' +
                            '</button>' +
                            "</li>" +
                            "</ul>" +
                            "</div>";
                    }
                }
            ]
        })
    })

    function formCrearAlumno() {
        remoteModal('/alumnos/create', 'modalCrearAlumno')
    }

    function formEditarAlumno(idAlumno) {
        remoteModal('/alumnos/' + idAlumno + '/edit', 'modalEditarAlumno')
    }

    function formCrearInscripcion(idAlumno) {
        remoteModal('/alumnos_inscribir/' + idAlumno, 'modalCrearInscripcion')
    }

    function eliminarAlumno(idAlumno) {
        Swal.fire({
            title: 'Estas seguro de eliminar el alumno?',
            text: 'No vas a poder revertir los cambios!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sii, borralo!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/alumnos/' + idAlumno,
                    method: 'DELETE',
                    success: function(data) {
                        Swal.fire(
                            'Eliminado!',
                            data.mensaje,
                            'success'
                        )
                        tablaAlumnos.DataTable().ajax.reload(null, false)
                    }
                })
            }
        })
    }

    $('#buscador').on('keyup', function(evente) {
        let tecla = event.keyCode;
        let valor = $('#buscador').val();

        if (valor == '') {
            tablaAlumnos.DataTable().search(this.value).draw();
        }

        if (tecla == '13') {
            tablaAlumnos.DataTable().search(this.value).draw();
        }
    });

    function longitudPagina() {
        let longitud = $('#longitudTabla').val();
        tablaAlumnos.DataTable().page.len(longitud).draw();
    };
</script>