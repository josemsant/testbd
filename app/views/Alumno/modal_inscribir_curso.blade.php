<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick=""><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title">Inscripcion a curso</h2>
        </div>
        <div class="modal-body">
            <div id="msj-error" class="alert alert-danger" role="alert" style="display: none;">
                <ul></ul>
            </div>
            <form id="formCrearInscripcion">
                <div class="form-group">
                    <div class="form-group">
                        <label for="cursos">Curso</label>
                        <input type="hidden" id="cursoInscripcion" name="cursos" class="form-control" style="width: 100%; padding: 0">
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="">Cerrar</button>
            <button type="button" class="btn btn-primary" id="crearInscripcion">Guardar</button>
        </div>
    </div>
</div>

<script>
    var $formularioInscripcion = $('#formCrearInscripcion')

    $(document).ready(function() {
        listaCursos('#cursoInscripcion');

        var cursos = {{$alumno->cursos_inscriptos}};
        
        var id = [];

        cursos.forEach(function(curso) {
            id.push(curso.id);
        });

        $('#cursoInscripcion').val(id);
        $('#cursoInscripcion').trigger('change');


        $formularioInscripcion.validate({
            ignore: [],
            rules: {
                cursos: {
                    required: true
                }
            },
            messages: {
                cursos: {
                    required: "El campo curso es obligatorio."
                },
            }
        });
    })

    $('#crearInscripcion').on('click', function(e) {
        e.preventDefault();
        var $validar = $formularioInscripcion.valid();

        if (!$validar) {
            $formularioInscripcion.validate().focusInvalid();
        } else {
            let elementoError = $('#msj-error ul');
            let divError = $('#msj-error');
            elementoError.html('');
            divError.hide();

            $.ajax({
                url: '/alumnos/{{$alumno->id}}/inscripcion',
                method: 'POST',
                data: $formularioInscripcion.serialize(),
                success: function(data, textStatus, jQxhr) {
                    Swal.fire({
                            icon: 'success',
                            title: data.mensaje,
                            showConfirmButton: false,
                            timer: 1500
                        }),
                        $("#modalCrearInscripcion .close").click();
                    tablaAlumnos.DataTable().ajax.reload(null, false)
                },
                error: function(jqXhr, textStatus, errorThrown) {
                    let {
                        status,
                        responseJSON
                    } = jqXhr

                    if (status === 409) {
                        divError.show();
                        Object.keys(responseJSON).forEach(function(key) {
                            var elemento = $('<li></li>').text(responseJSON[key])
                            elementoError.append(elemento);
                        })
                    }
                },
            })
        }
    });
</script>