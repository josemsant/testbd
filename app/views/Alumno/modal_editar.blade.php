<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrarForm"><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title" id="exampleModalLabel">Editar Alumno</h2>
        </div>
        <div class="modal-body">
            <div id="msj-error" class="alert alert-danger" role="alert" style="display: none;">
                <ul></ul>
            </div>
            <form id="formEditarAlumno">
                <div class="form-group">
                    <label for="nombre" class="control-label">nombre:</label>
                    <input type="text" name="nombre" class="form-control" value="{{$alumno->nombre}}">
                </div>
                <div class="form-group">
                    <label for="email" class="control-label">Email</label>
                    <input type="mail" name="email" class="form-control" value="{{$alumno->email}}">
                </div>
                <div class="form-group">
                    <label for="telefono" class="control-label">Telefono</label>
                    <input type="text" name="telefono" class="form-control" value="{{$alumno->telefono}}">
                </div>
                <div class="form-group">
                    <label for="direccion" class="control-label">Direccion</label>
                    <input type="text" name="direccion" class="form-control" value="{{$alumno->direccion}}">
                </div>
                <div class="form-group">
                    <label for="edad" class="control-label">Edad</label>
                    <input type="text" name="edad" class="form-control" value="{{$alumno->edad}}">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrarForm">Cerrar</button>
            <button type="button" class="btn btn-primary" id="editarAlumno">Editar</button>
        </div>
    </div>
</div>

<script>
    var $formularioEditar = $('#formEditarAlumno')

    $(document).ready(function() {

        // metodos personalizados de validacion
        $.validator.addMethod("soloLetras", function(value, element) {
            return this.optional(element) || /^[a-z ]+$/i.test(value);
        }, "El nombre solo puede tener letras.");

        $.validator.addMethod("soloNumeros", function(value, element) {
            return this.optional(element) || /^[0-9]+$/i.test(value);
        }, "El telefono solo puede tener numeros.");

        // Validaciones de campos del formulario
        $formularioEditar.validate({
            rules: {
                nombre: {
                    required: true,
                    soloLetras: true,
                    maxlength: 64
                },
                email: {
                    required: true,
                    email: true
                },
                telefono: {
                    required: true,
                    soloNumeros: true,
                    maxlength: 12
                },
                direccion: {
                    required: true,
                    maxlength: 64
                },
                edad: {
                    required: true,
                    min: 1,
                    max: 120,
                },
            },
            messages: {
                nombre: {
                    required: "El campo nombre es obligatorio.",
                    maxlength: "El campo no debe superar los 64 caracteres"
                },
                email: {
                    required: "El campo email es obligatorio.",
                    email: "El campo debe tener formato de email correcto."
                },
                telefono: {
                    required: "El campo telefono es obligatorio",
                    maxlength: "El campo no debe superar los 12 digitos"
                },
                direccion: {
                    required: "El campo es direccion obligatorio.",
                    maxlength: "El campo no debe superar los 64 caracteres"
                },
                edad: {
                    required: "El campo edad es obligatorio.",
                    min: "El campo edad debe ser mayor o igual a 1",
                    max: "El campo edad debe ser menor o igual a 120",
                },
            }
        });
    });

    $('#editarAlumno').on('click', function(e) {
        e.preventDefault();
        var $validar = $formularioEditar.valid();

        if (!$validar) {
            $formularioEditar.validate().focusInvalid();
        } else {
            let elementoError = $('#msj-error ul');
            let divError = $('#msj-error');
            elementoError.html('');
            divError.hide();

            $.ajax({
                url: '/alumnos/' + '{{$alumno->id}}',
                method: 'PUT',
                data: $formularioEditar.serialize(),
                success: function(data, textStatus, jQxhr) {
                    Swal.fire({
                            icon: 'success',
                            title: data.mensaje,
                            showConfirmButton: false,
                            timer: 1500
                        }),
                        $("#modalEditarAlumno .close").click();
                    tablaAlumnos.DataTable().ajax.reload(null, false);
                },
                error: function(jqXhr, textStatus, errorThrown) {
                    let {
                        status,
                        responseJSON
                    } = jqXhr

                    if (status === 409) {
                        divError.show();
                        Object.keys(responseJSON).forEach(function(key) {
                            var elemento = $('<li></li>').text(responseJSON[key])
                            elementoError.append(elemento);
                        })
                    }
                },
            })
        }
    });
</script>