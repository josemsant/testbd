<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Laravel PHP Framework</title>
</head>

<div class='btn-group'>
    <a class='btn btn-default btn-xs dropdown-toggle' data-toggle='dropdown' href='javascript:void(0);'><i class='fa fa-cog'></i></a>

    <ul class='dropdown-menu pull-right'>
        <li><a href='admin_usuarios/editar/${id}' data-toggle='modal-custom' class='actionEditItem'><i class='fa fa-edit'></i> Ver y Editar</a></li>
        <li><a href='admin_usuarios/tipo/${id}' data-toggle='modal-custom' class='actionTipo'><i class='fa fa-user'></i> Tipo y c&oacute;digo</a></li>
        <li><a href='javascript:void(0);' class='borrar_usuario' data-module='admin_usuarios' data-id='${id}'><i class='fa fa-trash-o'></i> Borrar</a></li>
    </ul>
</div>


<div class="dropdown">
    <button class="btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="fa fa-cog"></span>
        Acciones
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li><a href="#" onclick="formEditarAlumno(5000)"><span class="fa fa-edit" aria-hidden="true"></span> Editar</a></li>
        <li><a href=""><span class="fa fa-trash" aria-hidden="true"></span> Borrar</a></li>
    </ul>
</div>

<body>
    <h1>TestDB</h1>
    <ul>
        <li><a href="/pt1">Punto 1</a></li>
        <li><a href="/pt2">Punto 2</a></li>
        <li><a href="/pt3">Punto 3</a></li>
        <li><a href="/pt4">Punto 4</a></li>
        <li><a href="/pt5">Punto 5</a></li>
        <li><a href="/pt6">Punto 6</a></li>
        <li><a href="/pt7">Punto 7</a></li>
        <li><a href="/pt9">Punto 9</a></li>
        <li><a href="/pt12">Punto 12</a></li>
    </ul>

    <div class='btn-group text-left'>
        <a class='btn btn-default btn-xs dropdown-toggle' data-toggle='dropdown' href='javascript:void(0);'>
            <i class='fa fa-cog'></i>
        </a>
        <ul class='dropdown-menu pull-right'>
            <li>
                <a href='admin_tramites/editar/"+id+"' data-toggle='modal-custom' class='actionEditItem'>
                    <i class='fa fa-edit'></i>Ver y Editar
                </a>
            </li>
            <li>
                <a href='javascript:void(0);' class='borrar_tramite' data-id='"+id+"'>
                    <i class='fa fa-trash-o'></i>
                    Borrar
                </a>
            </li>
        </ul>
    </div>

</body>

</html>