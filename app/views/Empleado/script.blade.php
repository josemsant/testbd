<script>
    var tablaEmpleados = $('#tablaEmpleados');

    $(document).ready(function() {
        tablaEmpleados.DataTable({
            "processing": true,
            "serverSide": true,
            'pageLength': 5,
            "dom": 'tip',
            'ajax': {
                url: '/lista_empleados',
                method: 'GET'
            },
            "language": {
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "Search:": "Buscar:",
                "processing": "Cargando...",
                "emptyTable": "No hay datos disponibles",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "infoEmpty": "Mostrando 0 de 0 de 0 registros",
                "infoFiltered": "(fitrado de _MAX_ registros totales)",
                "zeroRecords": "No se encontraron coincidencias",
                "infoThousands": ",",
                "lengthMenu": "Mostrar _MENU_ registros",
            },
            'columnDefs': [{
                    bSearchable: true,
                    targets: [0],
                    data: 'nombre'
                },
                {
                    targets: [1],
                    data: 'direccion'
                },
                {
                    targets: [2],
                    data: 'telefono'
                },
                {
                    bSearchable: true,
                    targets: [3],
                    data: 'dni'
                },
                {
                    targets: [4],
                    data: 'sueldo'
                },
                {
                    targets: [5],
                    data: 'cargo'
                },
                {
                    targets: [6],
                    data: 'departamento'
                },
                {
                    data: null,
                    targets: [7],
                    mRender: function(data, type, fill) {
                        return "<div class='btn-group'>" +
                            "<a class='btn btn-default dropdown-toggle' data-toggle='dropdown' href='#'>" +
                            "<i class='fa fa-cog'></i> Acciones" +
                            "</a>" +
                            "<ul class='dropdown-menu pull-right'>" +
                            "<li>" +
                            '<button type="button" class="btn btn-warning" onclick="formEditarEmpleado(' + data.id + ')">' +
                            '<i class="fa fa-edit"></i> Editar' +
                            '</button>' +
                            "</li>" +
                            "<li>" +
                            '<button type="button" class="btn btn-danger" onclick="eliminarEmpleado(' + data.id + ')">' +
                            '<i class="fa fa-trash"></i> Borrar' +
                            '</button>' +
                            "</li>" +
                            "</ul>" +
                            "</div>";
                    }
                }
            ]
        })
    })

    function formCrearEmpleado() {
        remoteModal('/empleados/create', 'modalCrearEmpleado')
    };

    function formEditarEmpleado(idEmpleado) {
        remoteModal('/empleados/' + idEmpleado + '/edit', 'modalEditarEmpleado');
    };

    function eliminarEmpleado(idEmpleado) {
        Swal.fire({
            title: 'Estas seguro de eliminar el empleado?',
            text: 'No vas a poder revertir los cambios!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sii, borralo!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/empleados/' + idEmpleado,
                    method: 'DELETE',
                    success: function(data) {
                        Swal.fire(
                            'Eliminado!',
                            data.mensaje,
                            'success'
                        )
                        tablaEmpleados.DataTable().ajax.reload(null, true)
                    }
                })
            }
        })
    }

    $('#buscador').on('keyup', function(evente) {
        let tecla = event.keyCode;
        let valor = $('#buscador').val();

        if (valor == '') {
            tablaEmpleados.DataTable().search(this.value).draw();
        }

        if (tecla == '13') {
            tablaEmpleados.DataTable().search(this.value).draw();
        }

    });

    function longitudPagina() {
        let longitud = $('#longitudTabla').val();
        tablaEmpleados.DataTable().page.len(longitud).draw();
    };
</script>