<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap Recursos -->
    {{HTML::style('bootstrap/css/bootstrap.min.css')}}
    <!-- End Bootstrap Recursos -->

    <!-- FontAwesome Recursos -->
    {{HTML::style('FontAwesome/css/all.min.css')}}
    <!-- End FontAwesome Recursos -->

    <!-- Select 2 Recursos -->
    {{HTML::style('Select2/select2.css')}}
    {{HTML::style('Select2/select2-bootstrap.css')}}
    <!-- End Select 2 Recursos -->

    <!-- Admin Dash Recursos -->
    {{HTML::style('admin/favicon.ico')}}
    {{HTML::style('admin/css/dashboard.css')}}
    <!-- End Admin Dash Recursos -->

    <!-- Estilos Personalizados Recursos -->
    {{HTML::style('css/estilos.css')}}
    <!-- End Estilos Personalizados Recursos -->
</head>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Empleado: {{$empleado->nombre}}</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/logout">Salir</a></li>
            </ul>
        </div>
    </div>
</nav>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>Nombre: {{$empleado->nombre}}</h1>
            </div>
            <div class="panel-body">
                <h3>Informacion</h3>
                <ul>
                    <li>Dni: {{$empleado->dni}}</li>
                    <li>Telefono: {{$empleado->telefono}}</li>
                    <li>Direccion: {{$empleado->direccion}}</li>
                    <li>Edad: {{$empleado->edad}}</li>
                    <li>Sueldo: {{$empleado->sueldo}}</li>
                </ul>
                <h3>Cursos</h3>
                <ul>
                    
                    @foreach($empleado->cursos as $curso)
                        <li>Curso: {{$curso->nombre}}</li>
                    @endforeach
                </ul>
                <h3>Departamento</h3>
                <ul>
                
                    <li>{{$empleado->departamento->nombre}}</li>
                </ul>
            </div>
            <div class="panel-footer"></div>
        </div>
    </div>
    <!-- End Contenedor  -->
    <!-- scripts -->
    {{HTML::script('jquery/jquery-2.1.3.js')}}

    <!-- jQuery Validation -->
    {{HTML::script('jquery-validation/dist/jquery.validate.min.js')}}

    <!-- BootStrap 3 -->
    {{HTML::script('bootstrap/js/bootstrap.min.js')}}

    <!-- SweetAlert2 -->
    {{HTML::script('SweelAlert2/sweetalert2.all.min.js')}}

    <!-- Select 2 -->
    {{HTML::script('Select2/select2.min.js')}}

    @include('layouts.script')

    <script>

    </script>
    <!--End scripts -->
</body>

</html>