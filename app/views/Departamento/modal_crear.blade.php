<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick=""><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title">Nuevo Departamento</h2>
        </div>
        <div class="modal-body">
            <div id="msj-error" class="alert alert-danger" role="alert" style="display: none;">           
                <ul></ul>
            </div>
            <form id="formCrearDepartamento">
                <div class="form-group">
                    <label for="nombre" class="control-label">Nombre:</label>
                    <input type="text" name="nombre" class="form-control" id="nombre">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="">Cerrar</button>
            <button type="button" class="btn btn-primary" id="crearDepartamento">Guardar</button>
        </div>
    </div>
</div>

<script>
    var $formularioCrear = $('#formCrearDepartamento')

    $(document).ready(function() {

        // metodos personalizados de validacion
        $.validator.addMethod("soloLetras", function(value, element) {
            return this.optional(element) || /^[a-z ]+$/i.test(value);
        }, "El nombre solo puede tener letras.");

        // Validaciones de campos del formulario
        $formularioCrear.validate({
            rules: {
                nombre: {
                    required: true,
                    soloLetras: true,
                }
            },
            messages: {
                nombre: {
                    required: "El campo nombre es obligatorio.",
                },
            }
        });
    });

    $('#crearDepartamento').on('click', function(e) {
        e.preventDefault();
        var $validar = $formularioCrear.valid();

        if (!$validar) {
            $formularioCrear.validate().focusInvalid();
        } else {
            let elementoError = $('#msj-error ul');
            let divError = $('#msj-error');
            elementoError.html('');
            divError.hide();

            $.ajax({
                url: '/departamentos',
                method: 'POST',
                data: $formularioCrear.serialize(),
                success: function(data, textStatus, jQxhr) {
                    Swal.fire({
                            icon: 'success',
                            title: data.mensaje,
                            showConfirmButton: false,
                            timer: 1500
                        }),
                        $formularioCrear[0].reset();
                    tablaDepartamentos.DataTable().ajax.reload(null, false)
                },
                error: function(jqXhr, textStatus, errorThrown) {
                    let {
                        status,
                        responseJSON
                    } = jqXhr

                    if (status === 409) {
                        divError.show();
                        Object.keys(responseJSON).forEach(function(key) {
                            var elemento = $('<li></li>').text(responseJSON[key])
                            elementoError.append(elemento);
                        })
                    }
                },
            })
        }
    });
</script>