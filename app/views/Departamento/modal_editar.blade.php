<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrarForm"><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title" id="exampleModalLabel">Editar Departamnto</h2>
        </div>
        <div class="modal-body">
            <div id="msj-error" class="alert alert-danger" role="alert" style="display: none;">           
                <ul></ul>
            </div>
            <form id="formEditarDepartamento">
                <div class="form-group">
                    <label for="nombre" class="control-label">nombre:</label>
                    <input id="departamento" type="text" name="nombre" class="form-control" value="{{$departamento->nombre}}">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrarForm">Cerrar</button>
            <button type="button" class="btn btn-primary" id="editarDepartamento">Editar</button>
        </div>
    </div>
</div>

<script>
    var $formularioEditar = $('#formEditarDepartamento')

    $(document).ready(function() {

        // metodos personalizados de validacion
        $.validator.addMethod("soloLetras", function(value, element) {
            return this.optional(element) || /^[a-z ]+$/i.test(value);
        }, "El nombre solo puede tener letras.");

        // Validaciones de campos del formulario
        $formularioEditar.validate({
            rules: {
                nombre: {
                    required: true,
                    soloLetras: true,
                }
            },
            messages: {
                nombre: {
                    required: "El campo nombre es obligatorio.",
                },
            }
        });
    });

    // Acciones para editar y validar
    $('#editarDepartamento').on('click', function(e) {
        e.preventDefault();
        var $validar = $formularioEditar.valid();

        if (!$validar) {
            $formularioEditar.validate().focusInvalid();
        } else {
            let elementoError = $('#msj-error ul');
            let divError = $('#msj-error');

            elementoError.html('');
            divError.hide();

            $.ajax({
                url: '/departamentos/{{$departamento->id}}',
                method: 'PUT',
                data: $formularioEditar.serialize(),
                success: function(data, textStatus, jQxhr) {
                    Swal.fire({
                            icon: 'success',
                            title: data.mensaje,
                            showConfirmButton: false,
                            timer: 1500
                        }),
                    $("#modalEditarDepartamento .close").click();
                    tablaDepartamentos.DataTable().ajax.reload(null, false)
                },
                error: function(jqXhr, textStatus, errorThrown) {
                    let {
                        status,
                        responseJSON
                    } = jqXhr

                    if (status === 409) {
                        divError.show();
                        Object.keys(responseJSON).forEach(function(key) {
                            var elemento = $('<li></li>').text(responseJSON[key])
                            elementoError.append(elemento);
                        })
                    }
                },
            })
        }
    });
</script>