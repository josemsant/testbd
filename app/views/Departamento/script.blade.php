<script>
    var tablaDepartamentos = $('#tablaDepartamentos');

    $(document).ready(function() {
        tablaDepartamentos.DataTable({
            "processing": true,
            "serverSide": true,
            'pageLength': 5,
            "dom": 'tip',
            'ajax': {
                url: '/lista_departamentos',
                method: 'GET'
            },
            "language": {
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "Search:": "Buscar:",
                "processing": "Cargando...",
                "emptyTable": "No hay datos disponibles",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "infoEmpty": "Mostrando 0 de 0 de 0 registros",
                "infoFiltered": "(fitrado de _MAX_ registros totales)",
                "zeroRecords": "No se encontraron coincidencias",
                "infoThousands": ",",
                "lengthMenu": "Mostrar _MENU_ registros",
            },
            'columnDefs': [{
                    targets: [0],
                    data: 'id',
                },
                {
                    bSearchable: true,
                    targets: [1],
                    data: 'nombre'
                },
                {
                    data: null,
                    targets: [2],
                    mRender: function(data, type, fill) {
                        return "<div class='btn-group'>" +
                            "<a class='btn btn-default dropdown-toggle' data-toggle='dropdown' href='#'>" +
                            "<i class='fa fa-cog'></i> Acciones" +
                            "</a>" +
                            "<ul class='dropdown-menu pull-right'>" +
                            "<li>" +
                            '<button type="button" class="btn btn-warning" onclick="formEditarDepartamento(' + data.id + ')">' +
                            '<i class="fa fa-edit"></i> Editar' +
                            '</button>' +
                            "</li>" +
                            "<li>" +
                            '<button type="button" class="btn btn-danger" onclick="eliminarDepartamento(' + data.id + ')">' +
                            '<i class="fa fa-trash"></i> Borrar' +
                            '</button>' +
                            "</li>" +
                            "</ul>" +
                            "</div>";
                    }
                }
            ]
        })
    })

    function formCrearDepartameto() {
        remoteModal('/departamentos/create', 'modalCrearDepartamento')
    }

    function formEditarDepartamento(idDepartamento) {
        remoteModal('/departamentos/' + idDepartamento + '/edit', 'modalEditarDepartamento')
    }

    function eliminarDepartamento(idDepartamento) {
        Swal.fire({
            title: 'Estas seguro de eliminar la departamento?',
            text: 'No vas a poder revertir los cambios!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sii, borralo!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/departamentos/' + idDepartamento,
                    method: 'DELETE',
                    success: function(data) {
                        Swal.fire(
                            'Eliminado!',
                            data.mensaje,
                            'success'
                        )
                        tablaDepartamentos.DataTable().ajax.reload(null, false)
                    }
                })
            }
        })
    }

    $('#buscador').on('keyup', function(evente) {
        let tecla = event.keyCode;
        let valor = $('#buscador').val();

        if (valor == '') {
            tablaDepartamentos.DataTable().search(this.value).draw();
        }

        if (tecla == '13') {
            tablaDepartamentos.DataTable().search(this.value).draw();
        }
    });

    function longitudPagina() {
        let longitud = $('#longitudTabla').val();
        tablaDepartamentos.DataTable().page.len(longitud).draw();
    };
</script>