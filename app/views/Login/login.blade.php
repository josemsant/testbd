<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap Recursos -->
    {{HTML::style('bootstrap/css/bootstrap.min.css')}}
    <!-- End Bootstrap Recursos -->

    <!-- FontAwesome Recursos -->
    {{HTML::style('FontAwesome/css/all.min.css')}}
    <!-- End FontAwesome Recursos -->

    <!-- Admin Dash Recursos -->
    {{HTML::style('admin/favicon.ico')}}
    {{HTML::style('admin/css/dashboard.css')}}
    <!-- End Admin Dash Recursos -->

    <!-- Estilos Personalizados Recursos -->
    {{HTML::style('css/estilos.css')}}
    <!-- End Estilos Personalizados Recursos -->
</head>

<body>
    <div class="container">
        <div class="row">
            @if(Session::has('mensaje_error'))
            <div class="alert alert-danger fade in">
                <button class="close" data-dismiss="alert">×</button>
                <i class="fa-fw fa fa-times"></i>
                {{ Session::get('mensaje_error') }}
            </div>
            @endif

            @if(Session::has('mensaje'))
            <div class="alert alert-success fade in">
                <button class="close" data-dismiss="alert">×</button>
                <i class="fa-fw fa fa-times"></i>
                {{ Session::get('mensaje') }}
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-6 col-md-offset-4">
                                <h1>Login</h1>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                {{ Form::open(['url' => 'login', 'id' => 'login']) }}
                                <div class="form-group">
                                    <input type="text" name="usuario" id="usuario" tabindex="1" class="form-control" placeholder="Usuario" value="">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="contraseña" id="contraseña" tabindex="2" class="form-control" placeholder="Contraseña">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-5">
                                            {{Form::submit('Login', array('class' => 'btn btn-primary'))}}
                                        </div>
                                    </div>
                                </div>
                                <a href="/login/recuperar" style="text-decoration: none;" >Olvide mi contraseña</a>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Contenedor  -->
    <!-- scripts -->
    {{HTML::script('jquery/jquery-2.1.3.js')}}

    <!-- jQuery Validation -->
    {{HTML::script('jquery-validation/dist/jquery.validate.min.js')}}

    <!-- BootStrap 3 -->
    {{HTML::script('bootstrap/js/bootstrap.min.js')}}

    @include('layouts.script')

    <script>
        var $formularioLogin = $('#login');
        var $validador = $formularioLogin.validate({
            rules: {
                usuario: {
                    required: true,
                },
                contraseña: {
                    required: true,
                },
            },
            messages: {
                usuario: {
                    required: "Debes ingresar el usuario.",
                },
                contraseña: {
                    required: "Debes ingresar la contraseña.",
                }
            }
        })
    </script>
    <!--End scripts -->
</body>

</html>