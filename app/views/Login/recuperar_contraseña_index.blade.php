<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap Recursos -->
    {{HTML::style('bootstrap/css/bootstrap.min.css')}}
    <!-- End Bootstrap Recursos -->

    <!-- FontAwesome Recursos -->
    {{HTML::style('FontAwesome/css/all.min.css')}}
    <!-- End FontAwesome Recursos -->

    <!-- Admin Dash Recursos -->
    {{HTML::style('admin/favicon.ico')}}
    {{HTML::style('admin/css/dashboard.css')}}
    <!-- End Admin Dash Recursos -->

    <!-- Estilos Personalizados Recursos -->
    {{HTML::style('css/estilos.css')}}
    <!-- End Estilos Personalizados Recursos -->
</head>

<body>
    <div class="container">
        <div class="row">
            @if(Session::has('mensaje_error'))
            <div class="alert alert-danger fade in">
                <button class="close" data-dismiss="alert">×</button>
                <i class="fa-fw fa fa-times"></i>
                {{ Session::get('mensaje_error') }}
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="">
                                <h1>Recuperar Contraseña</h1>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                {{ Form::open(['url' => 'login/recuperar', 'id' => 'reset']) }}
                                <div class="form-group">
                                    <input type="text" name="dni" id="dni" tabindex="1" class="form-control" placeholder="Ingrese su DNI">
                                </div> 
                                <div class="form-group">
                                    <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="Ingrese su email">
                                </div>                                
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-5">
                                            <a href="/login" class="btn btn-default">Atras</a>
                                            {{Form::submit('Enviar', array('class' => 'btn btn-primary'))}}                                            
                                        </div>
                                    </div>
                                </div>                                
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Contenedor  -->
    <!-- scripts -->
    {{HTML::script('jquery/jquery-2.1.3.js')}}

    <!-- jQuery Validation -->
    {{HTML::script('jquery-validation/dist/jquery.validate.min.js')}}

    <!-- BootStrap 3 -->
    {{HTML::script('bootstrap/js/bootstrap.min.js')}}

    @include('layouts.script')

    <script>
        var $formularioLogin = $('#reset');
        var $validador = $formularioLogin.validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                dni: {
                    required: true,
                },
            },
            messages: {
                email: {
                    required: "Debes ingresar el Email.",
                    email: "Esto no es un email valido.",
                },
                dni: {
                    required: "Debes ingresar el DNI.",
                },
            }
        })
    </script>
    <!--End scripts -->
</body>

</html>