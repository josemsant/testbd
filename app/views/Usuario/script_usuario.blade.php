<script>
    var tablaUsuarios = $('#tablaUsuarios');

    $(document).ready(function() {
        tablaUsuarios.DataTable({
            "processing": true,
            "serverSide": true,
            'pageLength': 5,
            "dom": 'tip',
            'ajax': {
                url: '/lista_usuarios',
                method: 'GET'
            },
            "language": {
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "Search:": "Buscar:",
                "processing": "Cargando...",
                "emptyTable": "No hay datos disponibles",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "infoEmpty": "Mostrando 0 de 0 de 0 registros",
                "infoFiltered": "(fitrado de _MAX_ registros totales)",
                "zeroRecords": "No se encontraron coincidencias",
                "infoThousands": ",",
                "lengthMenu": "Mostrar _MENU_ registros",
            },
            'columnDefs': [{
                    bSearchable: true,
                    targets: [0],
                    data: 'id'
                },
                {
                    targets: [1],
                    data: 'nombre'
                },
                {
                    targets: [2],
                    data: 'dni'
                },                
                {
                    bSearchable: true,
                    targets: [3],
                    data: 'rol.nombre'
                },
                // {
                //     data: null,
                //     targets: [4],
                //     mRender: function(data, type, fill) {
                //         return "<div class='btn-group'>" +
                //             "<a class='btn btn-default dropdown-toggle' data-toggle='dropdown' href='#'>" +
                //             "<i class='fa fa-cog'></i> Acciones" +
                //             "</a>" +
                //             "<ul class='dropdown-menu pull-right'>" +
                //             "<li>" +
                //             '<button type="button" class="btn btn-danger" onclick="eliminarUsuarios(' + data.id + ')">' +
                //             '<i class="fa fa-trash"></i> Borrar' +
                //             '</button>' +
                //             "</li>" +
                //             "</ul>" +
                //             "</div>";
                //     }
                // }
            ]
        })
    })

    function formCrearUsuarioAlumno() {
        remoteModal('/usuarios/create_alumnos', 'modalCrearUsuarioAlumno')
    };

    function formCrearUsuarioEmpleado() {
        remoteModal('/usuarios/create_empleados', 'modalCrearUsuarioEmpleado')
    };    

    function formEditarEmpleado(idEmpleado) {
        remoteModal('/empleados/' + idEmpleado + '/edit', 'modalEditarEmpleado');
    };

    function eliminarEmpleado(idEmpleado) {
        Swal.fire({
            title: 'Estas seguro de eliminar el empleado?',
            text: 'No vas a poder revertir los cambios!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sii, borralo!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/empleados/' + idEmpleado,
                    method: 'DELETE',
                    success: function(data) {
                        Swal.fire(
                            'Eliminado!',
                            data.mensaje,
                            'success'
                        )
                        tablaUsuarios.DataTable().ajax.reload(null, true)
                    }
                })
            }
        })
    }

    $('#buscador').on('keyup', function(evente) {
        let tecla = event.keyCode;
        let valor = $('#buscador').val();

        if (valor == '') {
            tablaUsuarios.DataTable().search(this.value).draw();
        }

        if (tecla == '13') {
            tablaUsuarios.DataTable().search(this.value).draw();
        }
    });

    function longitudPagina() {
        let longitud = $('#longitudTabla').val();
        tablaUsuarios.DataTable().page.len(longitud).draw();
    };
</script>