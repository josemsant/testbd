@extends('layouts.master')

@section('title')
Usuarios
@endsection

@section('title page')
Lista de Usuarios
@endsection

@section('content')
<div class="form-group">
    <button type="button" class="btn btn-primary" onclick="formCrearUsuarioAlumno()">Nuevo Alumno</button>
    <button type="button" class="btn btn-primary" onclick="formCrearUsuarioEmpleado()">Nuevo Empleado</button>
</div>

<table id="tablaUsuarios" class="table table-striped table-bordered">
    <div class="col-xs-6 form-group">
        <label for="buscador">Buscador</label>
        <input type="text" name="buscardor" id="buscador" class="form-control">
    </div>

    <div class="col-xs-6 form-group">
        <label for="longitudTabla">Mostrar</label>
        <select name="longitudTabla" id="longitudTabla" class="form-control" onchange="longitudPagina()">
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
        </select>
    </div>
    <thead>
        <tr>
            <th>id</th>
            <th>Nombre</th>
            <th>Dni</th>           
            <th>rol</th>
            <!-- <th></th> -->
        </tr>
    </thead>
</table>
@endsection

@section('script_vista')
    @include('Usuario.script_usuario')
@endsection