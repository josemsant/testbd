<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick=""><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title">Nuevo Usuario</h2>
        </div>
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <a href="#" class="active btn btn-default" id="alumno">Alumno</a>
                </div>
                <div class="col-xs-6">
                    <a href="#" class="btn btn-default" id="empleado">Empleado</a>
                </div>
            </div>
            <hr>
        </div>
        <div class="modal-body">
            <div id="msj-error" class="alert alert-danger" role="alert" style="display: none;">
                <ul></ul>
            </div>
            <form id="formCrearUsuarioAlumno" style="display: block;">
                <div class="row">
                    <div class="col-sm-">
                        <div class="form-group">
                            <label for="nombre" class="control-label">Nombre</label>
                            <input type="text" name="nombre" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                            <input type="text" name="email" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="contraseña" class="control-label">Contraseña</label>
                            <input type="password" id="contraseña" name="contraseña" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="contraseña2" class="control-label">Repetir contraseña</label>
                            <input type="password" name="contraseña2" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="direccion" class="control-label">Direccion</label>
                            <input type="text" name="direccion" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="telefono" class="control-label">Telefono</label>
                            <input type="text" name="telefono" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="dni" class="control-label">D.N.I</label>
                            <input type="text" name="dni" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="edad" class="control-label">Edad</label>
                            <input type="text" name="edad" class="form-control">
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="crearUsuarioalumno">Guardar</button>
            </form>

            <form id="formCrearUsuarioEmpleado" style="display: none;">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="nombre" class="control-label">Nombre</label>
                            <input type="text" name="nombre" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                            <input type="text" name="email" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="contraseña" class="control-label">Contraseña</label>
                            <input type="password" id="contraseña" name="contraseña" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="contraseña2" class="control-label">Repetir contraseña</label>
                            <input type="password" name="contraseña2" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="direccion" class="control-label">Direccion</label>
                            <input type="text" name="direccion" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="telefono" class="control-label">Telefono</label>
                            <input type="text" name="telefono" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="dni" class="control-label">D.N.I</label>
                            <input type="text" name="dni" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="edad" class="control-label">Edad</label>
                            <input type="text" name="edad" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="sueldo" class="control-label">Sueldo</label>
                            <input type="text" name="sueldo" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="cargo">Cargo</label>
                            <input type="hidden" id="cargoCrear" name="cargo" class="form-control" style="width: 100%; padding: 0">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="departamento">Departamento</label>
                            <input type="hidden" id="departamentoCrear" name="departamento" class="form-control" style="width: 100%; padding: 0">
                        </div>
                    </div>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="crearUsuarioalumno">Guardar</button>
            </form>


        </div>
        <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary" id="crearUsuario">Guardar</button>
        </div> -->
    </div>
</div>
<script>

    $(document).ready(function() {
        // LLeno los select con departamentos y cargos disponbles para el empleado
        listaDepartamentos('#departamentoCrear');
        listaCargos('#cargoCrear');
        listaRoles('#rolCrear');
    });

    $(function() {

        $('#alumno').click(function(e) {
            $("#formCrearUsuarioAlumno").delay(100).fadeIn(100);
            $("#formCrearUsuarioEmpleado").fadeOut(100);
            $('#empleado').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });

        $('#empleado').click(function(e) {
            $("#formCrearUsuarioEmpleado").delay(100).fadeIn(100);
            $("#formCrearUsuarioAlumno").fadeOut(100);
            $('#alumno').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();

        });
    })
</script>