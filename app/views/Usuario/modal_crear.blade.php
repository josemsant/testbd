<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick=""><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title">Nuevo Usuario</h2>
        </div>
        <div class="modal-body">
            <div id="msj-error" class="alert alert-danger" role="alert" style="display: none;">
                <ul></ul>
            </div>
            <form id="formCrearUsuario">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="nombre" class="control-label">Nombre</label>
                            <input type="text" name="nombre" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                            <input type="text" name="email" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="contraseña" class="control-label">Contraseña</label>
                            <input type="password" id="contraseña" name="contraseña" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="contraseña2" class="control-label">Repetir contraseña</label>
                            <input type="password" name="contraseña2" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="direccion" class="control-label">Direccion</label>
                            <input type="text" name="direccion" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="telefono" class="control-label">Telefono</label>
                            <input type="text" name="telefono" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="dni" class="control-label">D.N.I</label>
                            <input type="text" name="dni" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="edad" class="control-label">Edad</label>
                            <input type="text" name="edad" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="sueldo" class="control-label">Sueldo</label>
                            <input type="text" name="sueldo" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="cargo">Cargo</label>
                            <input type="hidden" id="cargoCrear" name="cargo" class="form-control" style="width: 100%; padding: 0">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="departamento">Departamento</label>
                            <input type="hidden" id="departamentoCrear" name="departamento" class="form-control" style="width: 100%; padding: 0">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="rol">Rol</label>
                            <input type="hidden" id="rolCrear" name="rol" class="form-control" style="width: 100%; padding: 0">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="">Cerrar</button>
            <button type="button" class="btn btn-primary" id="crearUsuario">Guardar</button>
        </div>
    </div>
</div>

<script>
    var $formularioCrear = $('#formCrearUsuario')

    // metodos personalizados de validacion
    $.validator.addMethod("soloLetras", function(value, element) {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
    }, "El campo solo puede tener letras.");

    $.validator.addMethod("soloNumeros", function(value, element) {
        return this.optional(element) || /^[0-9]+$/i.test(value);
    }, "El campo solo puede tener numeros.");

    $.validator.addMethod("NumerosDecimales", function(value, element) {
        return this.optional(element) || /^-?\d*[.]?\d{0,2}$/i.test(value);
    }, "El campo solo puede tener numeros en formato decimal con dos digitos despues del punto.");

    // Validaciones de campos del formulario
    var $validador = $formularioCrear.validate({
        ignore: [],
        rules: {
            nombre: {
                required: true,
                soloLetras: true,
                maxlength: 64
            },
            email: {
                required: true,
                email: true
            },
            contraseña: {
                required: true,
            },
            contraseña2: {
                required: true,
                equalTo: '#contraseña',
            },
            direccion: {
                required: true,
                maxlength: 64
            },
            telefono: {
                required: true,
                soloNumeros: true,
                maxlength: 12
            },
            dni: {
                required: true,
                soloNumeros: true
            },
            sueldo: {
                required: true,
                NumerosDecimales: true
            },
            cargo: {
                required: true,
            },
            departamento: {
                required: true,
            },
            rol: {
                required: true,
            },
            edad: {
                required: true,
                min: 1,
                max: 120,
            },
        },
        messages: {
            nombre: {
                required: "El campo nombre es obligatorio.",
                maxlength: "El campo no debe superar los 64 caracteres"
            },
            email: {
                required: "El campo email es obligatorio.",
                email: "El campo debe tener formato de email correcto."
            },
            contraseña: {
                required: "El campo contraseña es obligatorio.",
            },
            contraseña2: {
                required: "El campo contraseña es obligatorio.",
                equalTo: "El campo no coincide con la contraseña ingresada.",
            },
            direccion: {
                required: "El campo es direccion obligatorio.",
                maxlength: "El campo no debe superar los 64 caracteres"
            },
            telefono: {
                required: "El campo telefono es obligatorio",
                maxlength: "El campo no debe superar los 12 digitos"
            },
            dni: {
                required: "El campo DNI es obligatorio.",
            },
            sueldo: {
                required: "El campo sueldo es obligatorio.",
            },
            cargo: {
                required: "El campo cargo es obligatorio.",
            },
            departamento: {
                required: "El campo departamento es obligatorio.",
            },
            rol: {
                required: "El campo rol es obligatorio.",
            },
            edad: {
                required: "El campo edad es obligatorio.",
                min: "El campo edad debe ser mayor o igual a 1",
                max: "El campo edad debe ser menor o igual a 120",
            },
        }
    });

    $(document).ready(function() {
        // LLeno los select con departamentos y cargos disponbles para el empleado
        listaDepartamentos('#departamentoCrear');
        listaCargos('#cargoCrear');
        listaRoles('#rolCrear');
    });

    $('#crearUsuario').on('click', function(e) {
        e.preventDefault();
        var $validar = $formularioCrear.valid();

        if (!$validar) {
            $validador.focusInvalid();
        } else {
            let elementoError = $('#msj-error ul');
            let divError = $('#msj-error');

            elementoError.html('');
            divError.hide();
            $.ajax({
                url: '/usuarios',
                method: 'POST',
                data: $formularioCrear.serialize(),
                success: function(data, textStatus, jQxhr) {
                    Swal.fire({
                            icon: 'success',
                            title: data.mensaje,
                            showConfirmButton: false,
                            timer: 1500
                        }),
                        $formularioCrear[0].reset();
                    tablaUsuarios.DataTable().ajax.reload(null, false)
                },
                error: function(jqXhr, textStatus, errorThrown) {
                    let {
                        status,
                        responseJSON
                    } = jqXhr

                    if (status === 409) {
                        divError.show();
                        Object.keys(responseJSON).forEach(function(key) {
                            var elemento = $('<li></li>').text(responseJSON[key])
                            elementoError.append(elemento);
                        })
                    }
                },
            })
        }
    });
</script>