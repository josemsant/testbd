<script>
    var tablaCursos = $('#tablaCursos');

    $(document).ready(function() {
        tablaCursos.DataTable({
            'processing': true,
            'serverSide': true,
            'pageLength': 5,
            'dom': 'tip',
            'ajax': {
                url: '/lista_cursos',
                method: 'GET'
            },
            "language": {
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "Search:": "Buscar:",
                "processing": "Cargando...",
                "emptyTable": "No hay datos disponibles",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "infoEmpty": "Mostrando 0 de 0 de 0 registros",
                "infoFiltered": "(fitrado de _MAX_ registros totales)",
                "zeroRecords": "No se encontraron coincidencias",
                "infoThousands": ",",
                "lengthMenu": "Mostrar _MENU_ registros",
            },
            'columnDefs': [{
                    bSearchable: true,
                    targets: [0],
                    data: 'nombre'
                },
                {
                    targets: [1],
                    data: 'precio'
                },
                {
                    targets: [2],
                    data: 'duracion'
                },
                {
                    targets: [3],
                    data: 'puntaje'
                },
                {
                    targets: [4],
                    data: 'periodo'
                },
                {
                    targets: [5],
                    data: 'docente'
                },
                {
                    targets: [6],
                    data: 'departamento'
                },
                {
                    data: null,
                    targets: [7],
                    mRender: function(data, type, fill) {
                        return "<div class='btn-group'>" +
                            "<a class='btn btn-default dropdown-toggle' data-toggle='dropdown' href='#'>" +
                            "<i class='fa fa-cog'></i> Acciones" +
                            "</a>" +
                            "<ul class='dropdown-menu pull-right'>" +
                            "<li>" +
                            '<button type="button" class="btn btn-warning" onclick="formEditarCurso(' + data.id + ')">' +
                            '<i class="fa fa-edit"></i> Editar' +
                            '</button>' +
                            "</li>" +
                            "<li>" +
                            '<button type="button" class="btn btn-danger" onclick="eliminarCurso(' + data.id + ')">' +
                            '<i class="fa fa-trash"></i> Borrar' +
                            '</button>' +
                            "</li>" +
                            "</ul>" +
                            "</div>";
                    }
                }
            ]
        })
    })

    function formCrearCurso() {
        remoteModal('/cursos/create', 'modalCrearCurso')
    };

    function formEditarCurso(idCurso) {
        remoteModal('/cursos/' + idCurso + '/edit', 'modalEditarCurso')
    }

    function eliminarCurso(idCurso) {
        Swal.fire({
            title: 'Estas seguro de eliminar el curso?',
            text: 'No vas a poder revertir los cambios!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sii, borralo!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/cursos/' + idCurso,
                    method: 'DELETE',
                    success: function(data) {
                        Swal.fire(
                            'Eliminado!',
                            data.mensaje,
                            'success'
                        )
                        tablaCursos.DataTable().ajax.reload()
                    }
                })
            }
        })
    }

    $('#buscador').on('keyup', function(evente) {
        let tecla = event.keyCode;
        let valor = $('#buscador').val();

        if (valor == '') {
            tablaCursos.DataTable().search(this.value).draw();
        }

        if (tecla == '13') {
            tablaCursos.DataTable().search(this.value).draw();
        }

    });

    function longitudPagina() {
        let longitud = $('#longitudTabla').val();
        tablaCursos.DataTable().page.len(longitud).draw();
    };
</script>