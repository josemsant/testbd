<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick=""><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title" id="exampleModalLabel">Nuevo Curso</h2>
        </div>
        <div class="modal-body">
            <div id="msj-error" class="alert alert-danger" role="alert" style="display: none;">
                <ul></ul>
            </div>
            <form id="formCrearCurso">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="nombre" class="control-label">Nombre</label>
                            <input type="text" name="nombre" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="domicilio" class="control-label">Precio</label>
                            <input type="text" name="precio" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="telefono" class="control-label">Duracion</label>
                            <input type="text" name="duracion" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="dni" class="control-label">Puntaje</label>
                            <input type="text" name="puntaje" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="departamento">Periodo</label>
                            <input type="hidden" id="periodoCrear" name="periodo" class="form-control" style="width: 100%; padding: 0">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="cargo">Docente</label>
                            <input type="hidden" id="docenteCrear" name="docente" class="form-control" style="width: 100%; padding: 0">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="departamento">Departamento</label>
                            <input type="hidden" id="departamentoCrear" name="departamento" class="form-control" style="width: 100%; padding: 0">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="">Cerrar</button>
            <button type="button" class="btn btn-primary" id="crearCurso">Guardar</button>
        </div>
    </div>
</div>

<script>
    var $formularioCrear = $('#formCrearCurso')

    $(document).ready(function() {
        listaDepartamentos('#departamentoCrear');
        listaDocentes('#docenteCrear');
        listaPeriodos('#periodoCrear');

        // metodos personalizados de validacion
        $.validator.addMethod("soloNumeros", function(value, element) {
            return this.optional(element) || /^[0-9]+$/i.test(value);
        }, "El campo solo puede tener numeros.");

        $.validator.addMethod("NumerosDecimales", function(value, element) {
            return this.optional(element) || /^-?\d*[.]?\d{0,2}$/i.test(value);
        }, "El campo solo puede tener numeros en formato decimal con dos digitos despues del punto.");

        // Validaciones de campos del formulario
        $formularioCrear.validate({
            ignore: [],
            rules: {
                nombre: {
                    required: true,
                    maxlength: 64
                },
                precio: {
                    required: true,
                    NumerosDecimales: true
                },
                duracion: {
                    required: true,
                    soloNumeros: true,
                },
                puntaje: {
                    required: true,
                    soloNumeros: true
                },
                periodo: {
                    required: true,
                },
                docente: {
                    required: true,
                },
                departamento: {
                    required: true,
                },
            },
            messages: {
                nombre: {
                    required: "El campo nombre es obligatorio.",
                    maxlength: "El campo no debe superar los 64 caracteres"
                },
                precio: {
                    required: "El campo es precio obligatorio.",
                },
                duracion: {
                    required: "El campo duracion es obligatorio",
                },
                puntaje: {
                    required: "El campo puntaje es obligatorio.",
                },
                periodo: {
                    required: "El campo periodo es obligatorio.",
                },
                docente: {
                    required: "El campo docente es obligatorio.",
                },
                departamento: {
                    required: "El campo departamento es obligatorio.",
                },
            }
        });
    });

    $('#crearCurso').on('click', function(e) {
        e.preventDefault();
        var $validar = $formularioCrear.valid();

        if (!$validar) {
            $formularioCrear.validate().focusInvalid();
        } else {
            let elementoError = $('#msj-error ul');
            let divError = $('#msj-error');
            elementoError.html('');
            divError.hide();

            $.ajax({                
                url: '/cursos',
                method: 'POST',
                data: $formularioCrear.serialize(),
                success: function(data, textStatus, jQxhr) {
                    Swal.fire({
                            icon: 'success',
                            title: data.mensaje,
                            showConfirmButton: false,
                            timer: 1500
                        }),
                        $("#modalCrearCurso .close").click()
                    tablaCursos.DataTable().ajax.reload()
                },
                error: function(jqXhr, textStatus, errorThrown) {
                    let {
                        status,
                        responseJSON
                    } = jqXhr

                    if (status === 409) {
                        divError.show();
                        Object.keys(responseJSON).forEach(function(key) {
                            var elemento = $('<li></li>').text(responseJSON[key])
                            elementoError.append(elemento);
                        })
                    }
                }
            })
        }
    });
</script>