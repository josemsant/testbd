<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick=""><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title" id="exampleModalLabel">Editar Curso</h2>
        </div>
        <div class="modal-body">
            <div id="msj-error" class="alert alert-danger" role="alert" style="display: none;">
                <ul></ul>
            </div>
            <form id="formEditarCurso">
                <input type="hidden" id="idCurso">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="nombre" class="control-label">Nombre</label>
                            <input type="text" id="nombre" name="nombre" class="form-control" value="{{$curso->nombre}}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="domicilio" class="control-label">Precio</label>
                            <input type="text" id="precio" name="precio" class="form-control" value="{{$curso->precio}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="telefono" class="control-label">Duracion</label>
                            <input type="text" id="duracion" name="duracion" class="form-control" value="{{$curso->duracion}}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="dni" class="control-label">Puntaje</label>
                            <input type="text" id="puntaje" name="puntaje" class="form-control" value="{{$curso->puntaje}}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="departamento">Periodo</label>
                            <input type="hidden" id="periodoEditar" name="periodo" class="form-control" style="width: 100%; padding: 0">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="cargo">Docente</label>
                            <input type="hidden" id="docenteEditar" name="docente" class="form-control" style="width: 100%; padding: 0">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="departamento">Departamento</label>
                            <input type="hidden" id="departamentoEditar" name="departamento" class="form-control" style="width: 100%; padding: 0">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="">Cerrar</button>
            <button type="button" class="btn btn-primary" id="editarCurso">Guardar</button>
        </div>
    </div>
</div>

<script>
    var $formularioEditar = $('#formEditarCurso')

    $(document).ready(function() {
        listaDepartamentos('#departamentoEditar');
        listaDocentes('#docenteEditar');
        listaPeriodos('#periodoEditar');

        $('#departamentoEditar').val('{{$curso->departamento->id}}');
        $('#departamentoEditar').trigger('change');

        $('#docenteEditar').val('{{$curso->docente->id}}');
        $('#docenteEditar').trigger('change');

        $('#periodoEditar').val('{{$curso->periodo}}');
        $('#periodoEditar').trigger('change');

        // metodos personalizados de validacion
        $.validator.addMethod("soloNumeros", function(value, element) {
            return this.optional(element) || /^[0-9]+$/i.test(value);
        }, "El campo solo puede tener numeros.");

        $.validator.addMethod("NumerosDecimales", function(value, element) {
            return this.optional(element) || /^-?\d*[.]?\d{0,2}$/i.test(value);
        }, "El campo solo puede tener numeros en formato decimal con dos digitos despues del punto.");

        // Validaciones de campos del formulario
        $formularioEditar.validate({
            ignore: [],
            rules: {
                nombre: {
                    required: true,
                    maxlength: 64
                },
                precio: {
                    required: true,
                    NumerosDecimales: true
                },
                duracion: {
                    required: true,
                    soloNumeros: true,
                },
                puntaje: {
                    required: true,
                    soloNumeros: true
                },
                periodo: {
                    required: true,
                },
                docente: {
                    required: true,
                },
                departamento: {
                    required: true,
                },
            },
            messages: {
                nombre: {
                    required: "El campo nombre es obligatorio.",
                    maxlength: "El campo no debe superar los 64 caracteres"
                },
                precio: {
                    required: "El campo es precio obligatorio.",
                },
                duracion: {
                    required: "El campo duracion es obligatorio",
                },
                puntaje: {
                    required: "El campo puntaje es obligatorio.",
                },
                periodo: {
                    required: "El campo periodo es obligatorio.",
                },
                docente: {
                    required: "El campo docente es obligatorio.",
                },
                departamento: {
                    required: "El campo departamento es obligatorio.",
                },
            }
        });
    });

    $('#editarCurso').on('click', function(e) {
        e.preventDefault();
        var $validar = $formularioEditar.valid();

        if (!$validar) {
            $formularioEditar.validate().focusInvalid();
        } else {
            let elementoError = $('#msj-error ul');
            let divError = $('#msj-error');
            elementoError.html('');
            divError.hide();

            $.ajax({
                url: '/cursos/{{$curso->id}}',
                method: 'PUT',
                data: $formularioEditar.serialize(),
                success: function(data, textStatus, jQxhr) {
                    Swal.fire({
                        icon: 'success',
                        title: data.mensaje,
                        showConfirmButton: false,
                        timer: 1500
                    })
                    $("#modalEditarCurso .close").click()
                    tablaCursos.DataTable().ajax.reload(null, false);

                },
                error: function(jqXhr, textStatus, errorThrown) {
                    let {
                        status,
                        responseJSON
                    } = jqXhr

                    if (status === 409) {
                        divError.show();
                        Object.keys(responseJSON).forEach(function(key) {
                            var elemento = $('<li></li>').text(responseJSON[key])
                            elementoError.append(elemento);
                        })
                    }
                }
            })
        }
    });
</script>