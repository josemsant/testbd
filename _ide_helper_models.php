<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace {
/**
 * Alumno
 *
 * @property integer $id 
 * @property string $nombre 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property-read Dictado $dictados 
 * @method static \Illuminate\Database\Query\Builder|\Alumno whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Alumno whereNombre($value)
 * @method static \Illuminate\Database\Query\Builder|\Alumno whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Alumno whereUpdatedAt($value)
 */
	class Alumno {}
}

namespace {
/**
 * Curso
 *
 * @property integer $id 
 * @property string $nombre 
 * @property float $precio 
 * @property boolean $duracion 
 * @property boolean $puntaje 
 * @property string $periodo 
 * @property integer $id_docente 
 * @property integer $id_departamento 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property-read Departamento: $departamento 
 * @property-read Empleado $docente 
 * @method static \Illuminate\Database\Query\Builder|\Curso whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Curso whereNombre($value)
 * @method static \Illuminate\Database\Query\Builder|\Curso wherePrecio($value)
 * @method static \Illuminate\Database\Query\Builder|\Curso whereDuracion($value)
 * @method static \Illuminate\Database\Query\Builder|\Curso wherePuntaje($value)
 * @method static \Illuminate\Database\Query\Builder|\Curso wherePeriodo($value)
 * @method static \Illuminate\Database\Query\Builder|\Curso whereIdDocente($value)
 * @method static \Illuminate\Database\Query\Builder|\Curso whereIdDepartamento($value)
 * @method static \Illuminate\Database\Query\Builder|\Curso whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Curso whereUpdatedAt($value)
 */
	class Curso {}
}

namespace {
/**
 * Departamento
 *
 * @property integer $id 
 * @property string $nombre 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property-read \Illuminate\Database\Eloquent\Collection|Curso[] $cursos 
 * @property-read \Illuminate\Database\Eloquent\Collection|Empleado:[] $empleados 
 * @method static \Illuminate\Database\Query\Builder|\Departamento whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Departamento whereNombre($value)
 * @method static \Illuminate\Database\Query\Builder|\Departamento whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Departamento whereUpdatedAt($value)
 */
	class Departamento {}
}

namespace {
/**
 * Dictado
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|Curso[] $cursos 
 * @property-read Empleado $docente 
 */
	class Dictado {}
}

namespace {
/**
 * Empleado
 *
 * @property integer $id 
 * @property string $nombre 
 * @property string $dni 
 * @property string $telefono 
 * @property string $domicilio 
 * @property float $sueldo 
 * @property string $cargo 
 * @property integer $id_departamento 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property-read Departamento $departamento 
 * @property-read \Illuminate\Database\Eloquent\Collection|Curso[] $cursos 
 * @method static \Illuminate\Database\Query\Builder|\Empleado whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Empleado whereNombre($value)
 * @method static \Illuminate\Database\Query\Builder|\Empleado whereDni($value)
 * @method static \Illuminate\Database\Query\Builder|\Empleado whereTelefono($value)
 * @method static \Illuminate\Database\Query\Builder|\Empleado whereDomicilio($value)
 * @method static \Illuminate\Database\Query\Builder|\Empleado whereSueldo($value)
 * @method static \Illuminate\Database\Query\Builder|\Empleado whereCargo($value)
 * @method static \Illuminate\Database\Query\Builder|\Empleado whereIdDepartamento($value)
 * @method static \Illuminate\Database\Query\Builder|\Empleado whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Empleado whereUpdatedAt($value)
 */
	class Empleado {}
}

namespace {
/**
 * Familiar
 *
 * @property integer $id 
 * @property string $nombre 
 * @property string $parentesco 
 * @property boolean $edad 
 * @property integer $id_empleado 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property-read Empleado $empleado 
 * @method static \Illuminate\Database\Query\Builder|\Familiar whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Familiar whereNombre($value)
 * @method static \Illuminate\Database\Query\Builder|\Familiar whereParentesco($value)
 * @method static \Illuminate\Database\Query\Builder|\Familiar whereEdad($value)
 * @method static \Illuminate\Database\Query\Builder|\Familiar whereIdEmpleado($value)
 * @method static \Illuminate\Database\Query\Builder|\Familiar whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Familiar whereUpdatedAt($value)
 */
	class Familiar {}
}

namespace {
/**
 * Inscripcion
 *
 * @property integer $id_alumno 
 * @property integer $id_curso 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property-read \Illuminate\Database\Eloquent\Collection|Curso[] $cursos 
 * @property-read \Illuminate\Database\Eloquent\Collection|Empleado[] $alumnos 
 * @method static \Illuminate\Database\Query\Builder|\Inscripcion whereIdAlumno($value)
 * @method static \Illuminate\Database\Query\Builder|\Inscripcion whereIdCurso($value)
 * @method static \Illuminate\Database\Query\Builder|\Inscripcion whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Inscripcion whereUpdatedAt($value)
 */
	class Inscripcion {}
}

